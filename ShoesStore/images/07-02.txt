﻿Giày Búp Bê Nữ
349,000 VND
Giày búp bê màu da của thương hiệu Zilandi có thiết kế đơn giản, tạo sự trẻ trung, năng động cho phái nữ.

- Giày có kích thước đúng tiêu chuẩn
- Chất liệu da tổng hợp
- Mũi giày được thiết kế phối quai chéo cách điệu
- Mũi giày kín

- Trắng