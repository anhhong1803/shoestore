http://www.zalora.vn/Giay-Chay-Bo-IGNITE-417188.html

Puma
Giày Chạy Bộ IGNITE
3,255,000 VND
Cho những buổi luyện tập của bạn thêm phần năng động và linh hoạt với giày chạy bộ từ thương hiệu PUMA. Bề mặt phối nhiều chất liệu từ vải lưới, đến da tổng hợp , cho đôi giày trọng lượng nhẹ và cảm giác thoáng khí trong từng bước di chuyển.

- Giày có kích cỡ đúng tiêu chuẩn
- Chất liệu vải, da tổng hợp
- Mũi giày tròn
- Thiết kế thắt dây
- Lưỡi giày phối tên và logo thương hiệu
- Cổ giày phối đệm mút bảo vệ cổ chân
- Mặt lót giày in tên thương hiệu

Với sản phẩm màu neon, màu thực tế có thể chênh lệch so với hình ảnh, tuỳ theo độ phân giải màu sắc do máy tính hiển thị

xanh dương