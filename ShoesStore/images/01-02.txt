http://www.zalora.vn/Giay-Luyen-Tap-Nike-Zoom-Speed-Tr2-393847.html

Nike
Giày Luyện Tập Nike Zoom Speed Tr2
2,282,000 VND
Thoải mái và năng động với giày luyện tập của thương hiệu Nike. Thiết kế ôm gọn chân giúp bạn dễ dàng di chuyển hơn.

- Giày có kích cỡ đúng tiêu chuẩn
- Chất liệu da tổng hợp, vải lưới
- Mũi tròn
- Thiết kế thắt dây
- Đệm mút bên trong giày
- In logo bên hông
- Đế bằng cao su XDR tăng cường độ bền khi hoạt động mạnh

Xanh dương