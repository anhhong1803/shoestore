http://www.zalora.vn/Giay-The-Thao-Match-74-417189.html



Puma
Giày Thể Thao Match 74
2,155,000 VND
Giày thể thao từ thương hiệu PUMA được thiết kế tỉ mỉ và tinh tế đến từng chi tiết, phối lỗ thoáng khí, mang đến cảm giác tự tin và năng động trong những hoạt động ngoài trời. Màu sắc đơn giản, trang nhã, phối tên thương hiệu in ở phần hông và lưỡi giày, khẳng định đẳng cấp.

- Chất liệu da tổng hợp
- Mũi giày tròn
- Thiết kế thắt dây
- May viền chỉ nổi quanh giày
- Cổ giày phối đệm mút bảo vệ
- Mặt lót giày in tên thương hiệu

trắng