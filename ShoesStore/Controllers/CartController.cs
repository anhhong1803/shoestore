﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.Owin;
using ShoesStore.Models.Services;
using Microsoft.Owin.Security;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.EF;

namespace ShoesStore.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private CartService cartService;


        public CartController()
        {
            //cartService = CartService.GetCart(this.HttpContext);
        }

        // GET: Cart
        [AllowAnonymous]
        public ActionResult AddToCart(int productId, double price, int quantity, int sizeId, int colorId)
        {
            cartService = CartService.GetCart(this.HttpContext);
            int succeesedQuantity = cartService.AddToCart(productId, price, quantity, sizeId, colorId);

            return RedirectToAction("CartDetail");
        }

        [AllowAnonymous]
        public ActionResult CartDetail()
        {
            TempData["Editable"] = true;
            return View();
        }

        [AllowAnonymous]
        public ActionResult CartDetailPartial()
        {
            cartService = CartService.GetCart(this.HttpContext);

            ViewBag.Editable = TempData["Editable"];

            CartDetailViewModel model = new CartDetailViewModel();
            model.CartItems = cartService.GetCartItems();

            model.TotalQuanity = 0;
            model.TotalPrice = 0;
            if (model.CartItems != null)
            {
                foreach (  CartItem item in model.CartItems)
                {
                    model.TotalQuanity += item.Quanity;
                    model.TotalPrice += (item.Price*item.Quanity);
                }
            }
            return PartialView(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ChangeQuantity(int itemId, int quantity)
        {
            cartService = CartService.GetCart(this.HttpContext);

            int result = cartService.ChangeQuantity(itemId, quantity);
            TempData["Editable"] = true;
            //return "{\"quantity\": \""+ result +"\", \"itemId\": \"" + itemId + "\"}";
            return RedirectToAction("CartDetailPartial");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult RemoveFromCart(int? itemId)
        {
            cartService = CartService.GetCart(this.HttpContext);

            int result = cartService.RemoveFromCart(itemId);
            TempData["Editable"] = true;

            return RedirectToAction("CartDetailPartial");
        }

        [Authorize(Roles="User, Admin, Mod")]
        public async Task<ActionResult> CheckOut()
        {
            cartService = CartService.GetCart(this.HttpContext);

            var cart = cartService.GetCartItems();
            if (cart.Count == 0)
            {
                return RedirectToAction("CartDetail");
            }

            var user =  await UserManager.FindByNameAsync(User.Identity.Name);
            CheckOutViewModel model = new CheckOutViewModel()
            {
                Name = user.Name,
                Email = user.Email,
                Address = user.Address,
                Phone = user.PhoneNumber
            };
            TempData["Editable"] = false;
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "User, Admin, Mod")]
        public async Task<ActionResult> CheckOut(CheckOutViewModel model)
        {
            cartService = CartService.GetCart(this.HttpContext);
            List<CartItem> lstCartItems = cartService.GetCartItems();
            var user = await UserManager.FindByNameAsync(User.Identity.Name);

            if (!model.IsOnePerson)
            {
                if (ModelState.IsValid)
                {

                    Order order = cartService.CreateOrder(user.Id, model);
                    if (order != null)
                    {
                        await SendConfirmOrderMail(order);

                    ViewBag.Message = "Đơn hàng của bạn đã được đặt thành công. Chi tiết đơn hàng đã được gửi đến Email của bạn!";
                    return View("Notification");
                    }
                    else
                    {
                        ModelState.AddModelError("",
                            "Một hoặc nhiều sản phẩm bạn chọn đã được khách hàng khác mua, vui lòng quay lại trang Chi tiết giỏ hàng để cập nhật!");
                        return View(model);
                    }
                }
                else
                {
                    return View(model);
                }
            }
            else
            {
                 Order order = cartService.CreateOrder(user.Id);

                if (order != null)
                    {
                        await SendConfirmOrderMail(order);

                    ViewBag.Message = "Đơn hàng của bạn đã được đặt thành công. Chi tiết đơn hàng đã được gửi đến Email của bạn!";
                    return View("Notification");
                    }
                    else
                {
                    ModelState.AddModelError("",
                        "Một hoặc nhiều sản phẩm bạn chọn đã được khách hàng khác mua, vui lòng quay lại trang Chi tiết giỏ hàng để cập nhật!");
                        return View(model);
                    }
            }
        }

        private async Task SendConfirmOrderMail(Order order)
        {
            Recipient rep = order.Recipient;
            string repInfo = "";
            if (rep!=null)
            {
                repInfo += "<div>" +
                           "<h3>Thông tin người nhận</h3>" +
                           "<p> Họ Tên: " + rep.Name + "<br>" +
                           "Địa Chỉ: " + rep.Address + "<br>" +
                           "Email: " + rep.Email + "<br>" +
                           "Điện Thoại: " + rep.PhoneNumber + "</p>";
            }

            var cartItems =  order.OrderDetails.Select(c=>c).ToList<OrderDetail>();
            string orderDetail = "<table border=\"1em\">" +
                                 "<thead>" +
                                 "<tr>" +
                                 "<th><strong>#</strong></th>" +
                                 "<th><strong>Tên sản phẩm</strong></th>" +
                                 "<th><strong>Màu Sắc</strong></th>" +
                                 "<th><strong>Size</strong></th>" +
                                 "<th><strong>Số Lượng</strong></th>" +
                                 "<th><strong>Đơn Giá</strong></th>" +
                                 "</tr" +
                                 "</thead>" +
                                 "<tbody>";

            ShoeStoreEntities shoeStoreEntities = new ShoeStoreEntities();
            for (int i = 0; i < cartItems.Count; i++)
            {
                int procId = cartItems[i].IDProduct;
                var singleOrDefault = shoeStoreEntities.Products.SingleOrDefault(p=>(p.ID == procId));
                if (singleOrDefault != null)
                    orderDetail += "<tr>" +
                                   "<td>"+ (i+1).ToString() +"</td>" +
                                   "<td>" + singleOrDefault.ProductName + "</td>" +
                                   "<td>" + singleOrDefault.Color.ColorName + "</td>" +
                                   "<td>" + singleOrDefault.Size.SizeName + "</td>" +
                                   "<td>" + cartItems[i].Quantity.ToString() + "</td>" +
                                   "<td>" + cartItems[i].Price.ToString("C0", CultureInfo.CreateSpecificCulture("vi-VN")) + "</td>" +
                                   "</tr>";
            }
			
			orderDetail += "<tr>" +
                                   "<td>"+ "</td>" +
                                   "<td>" + "</td>" +
                                   "<td>" +  "</td>" +
                                   "<td>" + "</td>" +
                                   "<td>" + "Tổng Cộng:"+"</td>" +
                                   "<td>" + order.Total.ToString("C0", CultureInfo.CreateSpecificCulture("vi-VN")) + "</td>" +
                                   "</tr";
            orderDetail += "</tbody>" +
                           "</table>";

                await
                    UserManager.SendEmailAsync(order.IDUser, "Chi Tiết Đơn Hàng", "<html><h2>Xác Nhận Đơn Hàng</h2><h3>Đơn hàng số #"+order.ID+"</h3><div>Ngày đặt: "+order.Date+"</div>"+repInfo+"<h3>Chi Tiết Đơn Hàng</h3>"+orderDetail+"</html>");
        }
    }
}