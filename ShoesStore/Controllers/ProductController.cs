﻿using ShoesStore.Models;
using ShoesStore.Models.Services;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShoesStore.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private ShoeStoreEntities db = new ShoeStoreEntities();

        // GET: Category
        // Nhận từ trang chủ: idCategory và idMenu 1: nam, 2: nu, 3: thuong hieu, 4: hàng mới, 5: khuyến mãi-->
        // trang chủ gọi đường link: @Html.ActionLink("ten danh muc", "Category", "Product", new {id = idCategory, idMenu=m})
        [AllowAnonymous]
        public ActionResult Category(int? idCategory, int? idMenu, int? productPage, int? page, int? arrange)
        {         
            if (!productPage.HasValue)
            {
                productPage = 3; // ProductPerPage
            }
            if (!page.HasValue)
            {
                page = 1; // CurrentPage
            }
            if (!arrange.HasValue)
            {
                arrange = 1;
            }
            if (!idMenu.HasValue)
            {
                idMenu = 4;
            }
           
            ViewBag.page = page;
            ViewBag.productPage = productPage;
            ViewBag.arrange = arrange;

            ProductService psv = new ProductService();

            if (idMenu.HasValue)
            {
                ViewBag.idMenu = idMenu;
                //Lấy tên menu được chọn
                switch (idMenu)
                {
                    case 1: ViewBag.MenuName = "Giày nam"; break;
                    case 2: ViewBag.MenuName = "Giày nữ"; break;
                    case 3: ViewBag.MenuName = "Thương hiệu"; break;
                    case 4: ViewBag.MenuName = "Hàng mới"; break;
                    case 5: ViewBag.MenuName = "Khuyến mãi"; break;
                    default: break;
                }

                List<ProductViewModel> lstProduct = new List<ProductViewModel>();
                // Tìm danh sách sp theo Nam, Nữ
                if (idMenu == 1 || idMenu == 2)
                {
                    if (idCategory.HasValue)
                    {
                        ViewBag.idCategory = idCategory;

                        lstProduct = psv.GetAllProductByCategory((int)idCategory, (int)idMenu, (int)productPage, (int)page, (int)arrange);

                        // Tổng số sản phẩm tìm được
                        List<ProductViewModel> lstProduct1 = psv.GetAllProductByCategory((int)idCategory, (int)idMenu);
                        ViewBag.numberOfProduct = lstProduct1.Count;

                        string SubMenuName = psv.GetCategoryName((int)idCategory);
                        ViewBag.SubMenuName = SubMenuName;
                    }
                }

                // Tìm danh sách sp theo thương hiệu, idCategory là idBrand
                if (idMenu == 3)
                {
                    ViewBag.idCategory = idCategory;
                    lstProduct = psv.GetAllProductByBrand((int)idCategory, (int)productPage, (int)page, (int)arrange);

                    // Tổng số sản phẩm tìm được
                    List<ProductViewModel> lstProduct1 = psv.GetAllProductByBrand((int)idCategory);
                    ViewBag.numberOfProduct = lstProduct1.Count;

                    string SubMenuName = psv.GetBrandName((int)idCategory);
                    ViewBag.SubMenuName = SubMenuName;

                }

                // Tìm danh sách sp theo hàng mới
                if (idMenu == 4)
                {
                    ViewBag.idCategory = 0;
                    lstProduct = psv.GetAllNewProduct((int)productPage, (int)page, (int)arrange);

                    // Tổng số sản phẩm tìm được
                    List<ProductViewModel> lstProduct1 = psv.GetAllNewProduct();
                    ViewBag.numberOfProduct = lstProduct1.Count;

                    string SubMenuName = null;
                    ViewBag.SubMenuName = SubMenuName;

                }

                // Tìm danh sách sp theo khuyến mãi
                if (idMenu == 5)
                {
                    ViewBag.idCategory = 0;
                    lstProduct = psv.GetAllSaleOffProduct((int)productPage, (int)page, (int)arrange);

                    // Tổng số sản phẩm tìm được
                    List<ProductViewModel> lstProduct1 = psv.GetAllSaleOffProduct();
                    ViewBag.numberOfProduct = lstProduct1.Count;

                    string SubMenuName = null;
                    ViewBag.SubMenuName = SubMenuName;
                }

                return View(lstProduct);
            }


            return View(new List<ProductViewModel>());
        }

        // GET: Details
        [AllowAnonymous]
        public ActionResult Details(int? idProduct)
        {
            string notice = null;
            if (!idProduct.HasValue)
            {
                idProduct = 2;
            }
            // Tìm thông tin sản phẩm
            ProductService psv = new ProductService();
            ProductViewModel product = psv.GetProduct((int)idProduct);

            // Lấy tên màu
            string color = psv.GetColor(product.IDColor);
            ViewBag.color = color;

            // Tên thương hiệu
            string brand = psv.GetBrand(product.IDBrand);
            ViewBag.brand = brand;

            // Size
            string size = psv.GetSize(product.IDSize);
            ViewBag.size = size;

            // Loại sản phẩm
            string category = psv.GetCategory(product.IDCategory);
            ViewBag.category = category;

            // Cập nhật Lượt xem
            ViewBag.viewed = (product.Viewed + 1).ToString();
            notice = psv.UpdateViewed(product.ID);
       
            // Lấy danh sách sản phẩm tương tự
            List<ProductViewModel> similarProduct = psv.GetAllProduct(product.IDCategory);
            ViewBag.lstSimilarProduct = similarProduct;

            ViewBag.Notice = notice;
            return View(product);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Index(int? numPerPage, int? page)
        {
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;

            // Lấy tất cả sản phẩm theo phân trang
            ProductService psv = new ProductService();
            List<ViewProductViewModel> lstProduct = new List<ViewProductViewModel>();
            lstProduct = psv.GetAllProduct((int)numPerPage, (int)page);
            // Lấy số lượng sản phẩm
            List<ViewProductViewModel> lstProduct1 = new List<ViewProductViewModel>();
            lstProduct1 = psv.GetAllProduct();

            ViewBag.PageCount = (lstProduct1.Count % numPerPage) == 0 ? lstProduct1.Count / numPerPage : (lstProduct1.Count / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumProduct = lstProduct1.Count;

            return View(lstProduct);
        }

        // Thêm sản phẩm mới
        [HttpPost]
        [Authorize(Roles="Admin")]
        public ActionResult Add(AddProductViewModel product, HttpPostedFileBase image, HttpPostedFileBase largeImage)
        {
            string notice = null;
            int i = product.ID;
            string pathImage = null;
            string pathLargeImage = null;
            string path1 = null, path2 = null;

            if (ModelState.IsValid)
            {
                ProductService psv = new ProductService();
                CategoryService csv = new CategoryService();
                
                ViewBag.lstCategory = getAllCate();
                // Danh sách Brand
                
                ViewBag.lstBrand = getAllBrand();
                // Danh sách Color
                
                ViewBag.lstColor = getAllColor();
                // Danh sách Size
                
                ViewBag.lstSize = getAllSize();

                
                ViewBag.SubTitle = "Thông tin sản phẩm";
                if (image != null && largeImage != null)
                {
                    string pic1 = System.IO.Path.GetFileName(image.FileName);
                    pathImage = System.IO.Path.Combine(
                                            Server.MapPath("~/images/"), pic1);
                    // file is uploaded
                    image.SaveAs(pathImage);
                    path1 = "/images/" + pic1;

                    string pic2 = System.IO.Path.GetFileName(largeImage.FileName);
                    pathLargeImage = System.IO.Path.Combine(
                                            Server.MapPath("~/images/"), pic2);
                    // file is uploaded
                    image.SaveAs(pathLargeImage);
                    path2 = "/images/" + pic2;

                    notice = psv.AddProduct(product.ProductName, product.IDBrand, product.IDCategory, product.IDColor,
                    product.IDSize, product.SaleOff, product.Price, product.Quantity, product.Describe,
                    path1, path2, product.MenWomen);
                }
                else
                {
                    notice = "Hình đại diện và hình chi tiết không được rỗng.";
                }
                
                ViewBag.Notice = notice;
                return View();
            }
            else
            {

                return View(product);
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Add()
        {
            
            // tìm danh sách Category
            ViewBag.lstCategory = getAllCate();
            // Danh sách Brand

            ViewBag.lstBrand = getAllBrand();
            // Danh sách Color

            ViewBag.lstColor = getAllColor();
            // Danh sách Size

            ViewBag.lstSize = getAllSize();

           
            ViewBag.SubTitle = "Thông tin sản phẩm";
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? idProduct)
        {
            // tìm danh sách Category
            ViewBag.lstCategory = getAllCate();
            // Danh sách Brand

            ViewBag.lstBrand = getAllBrand();
            // Danh sách Color

            ViewBag.lstColor = getAllColor();
            // Danh sách Size

            ViewBag.lstSize = getAllSize();

           
            ViewBag.SubTitle = "Cập nhật thông tin sản phẩm";
            if (idProduct.HasValue)
            {
                // Tìm product có id tương ứng
                ProductService psv = new ProductService();
                AddProductViewModel product = new AddProductViewModel();
                product = psv.FindProductById((int)idProduct);
                return View(product);
            }


            return View(new AddProductViewModel());
        }

        // Cập nhật sản phẩm 
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(AddProductViewModel product, HttpPostedFileBase image, HttpPostedFileBase largeImage)
        {
            string notice = null;
            int i = product.ID;
            string pathImage = null;
            string pathLargeImage = null;
            string path1 = null, path2 = null;

            if (ModelState.IsValid)
            {
                ProductService psv = new ProductService();
                CategoryService csv = new CategoryService();

                ViewBag.lstCategory = getAllCate();
                // Danh sách Brand

                ViewBag.lstBrand = getAllBrand();
                // Danh sách Color

                ViewBag.lstColor = getAllColor();
                // Danh sách Size

                ViewBag.lstSize = getAllSize();

                // Cập nhật thông tin sản phẩm
                if (i != 0)
                {
                    ViewBag.SubTitle = "Cập nhật thông tin  sản phẩm";
                    if (image != null && largeImage != null)
                    {
                        string pic1 = System.IO.Path.GetFileName(image.FileName);
                        pathImage = System.IO.Path.Combine(
                                                Server.MapPath("~/images/"), pic1);
                        // file is uploaded
                        image.SaveAs(pathImage);
                        path1 = "/images/" + pic1;

                        string pic2 = System.IO.Path.GetFileName(largeImage.FileName);
                        pathLargeImage = System.IO.Path.Combine(
                                                Server.MapPath("~/images/"), pic2);
                        // file is uploaded
                        image.SaveAs(pathLargeImage);
                        path2 = "/images/" + pic2;

                        notice = psv.EditProduct(product.ID, product.ProductName, product.IDBrand, product.IDCategory, product.IDColor,
                        product.IDSize, product.SaleOff, product.Price, product.Quantity, product.Describe, product.Viewed, product.NewProduct,
                        path1, path2, product.MenWomen);
                    }
                    else
                    {
                        notice = "Hình đại diện và hình chi tiết không được rỗng.";
                    }
                }

                ViewBag.Notice = notice;
                return View(product);
            }
            else
            {

                return View(product);
            }
        }
        
        // Xóa sản phẩm
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string idProduct)
        {
            int id = Int32.Parse(idProduct);
            ProductService psv = new ProductService();
            psv.DeleteProduct(id);
            return RedirectToAction("Index", "Product");
        }


        // GET: Product/Comment
        [AllowAnonymous]
        public ActionResult Comment(int idProduct)
        {
            CommentServicecs cs = new CommentServicecs();
            var result = cs.GetAllComment(idProduct);
            var json = Json(result, JsonRequestBehavior.AllowGet);
            return json;
        }

        // POST: Product/Comment
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Comment(CommentViewModel comment)
        {
            CommentServicecs cs = new CommentServicecs();
            comment.Date = DateTime.Now;
            comment.Status = 0;
            
            var result = cs.AddComment(comment);

            return PartialView("CommentPartail", comment);
        }

        // Support Method      
        private List<SelectListItem> getAllCate()
        {
            ProductService psv = new ProductService();
            CategoryService csv = new CategoryService();
            // tìm danh sách Category
            List<CategoryViewModel> lstCategory = csv.GetAllCategory();
            List<SelectListItem> lstSeCategory = new List<SelectListItem>();
            foreach (CategoryViewModel cate in lstCategory)
            {
               
                lstSeCategory.Add(new SelectListItem { Text = cate.CategoryName, Value = cate.ID.ToString() });
            }
            return lstSeCategory;
        }

        private List<SelectListItem> getAllColor()
        {
            ProductService psv = new ProductService();
            CategoryService csv = new CategoryService();
            // tìm danh sách Category
            List<ColorViewModel> lstColor = csv.GetAllColor();
            List<SelectListItem> lstSeColor = new List<SelectListItem>();
            foreach (ColorViewModel color in lstColor)
            {
                lstSeColor.Add(new SelectListItem { Text = color.ColorName, Value = color.ID.ToString() });
            }
            return lstSeColor;
        }

        private List<SelectListItem> getAllBrand()
        {
            ProductService psv = new ProductService();
            CategoryService csv = new CategoryService();
            // tìm danh sách Category
            List<BrandViewModel> lstBrand = csv.GetAllBrand();
            List<SelectListItem> lstSeBrand = new List<SelectListItem>();
            foreach (BrandViewModel brand in lstBrand)
            {
                lstSeBrand.Add(new SelectListItem { Text = brand.BrandName, Value = brand.ID.ToString() });
            }
            return lstSeBrand;
        }
        private List<SelectListItem> getAllSize()
        {
            ProductService psv = new ProductService();
            CategoryService csv = new CategoryService();
            // tìm danh sách Category
            List<SizeViewModel> lstSize = csv.GetAllSize();
            List<SelectListItem> lstSeSize = new List<SelectListItem>();
            foreach (SizeViewModel size in lstSize)
            {
                lstSeSize.Add(new SelectListItem { Text = size.SizeName, Value = size.ID.ToString() });
            }
            return lstSeSize;
        }

        
    }
}