﻿using System.Globalization;
using ShoesStore.Models.Services;
using ShoesStore.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ShoesStore.Controllers
{
    public class StatisticsController : Controller
    {
        // GET: Statistics
        [Authorize(Roles = "Admin")]
        public ActionResult StatisticsGeneral()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        public string StatisticsDay(String strDate = null)
        {
            //ObjectStatistics obj1 = new ObjectStatistics("Z", 29.765957771107);
            //ObjectStatistics obj2 = new ObjectStatistics("B", 0);
            //ObjectStatistics obj3 = new ObjectStatistics("C", 32.807804682612);
            //ObjectStatistics obj4 = new ObjectStatistics("D", 196.45946739256);
            //ObjectStatistics obj5 = new ObjectStatistics("E", 0.19434030906893);

            //List<ObjectStatistics> lst = new List<ObjectStatistics>();
            //lst.Add(obj1);
            //lst.Add(obj2);
            //lst.Add(obj3);
            //lst.Add(obj4);
            //lst.Add(obj5);
            if(strDate == "")
            {
                strDate = DateTime.Now.ToShortDateString();
            }
            DateTime date = DateTime.Parse(strDate);
            StatisticsService ss = new StatisticsService();
            List<ObjectStatistics> lst = ss.StatisticsDay(date);
            string strJson = convertToJsonString(lst);

            return strJson;
        }
        [Authorize(Roles = "Admin")]
        public string StatisticsMonth(String strDate = null)
        {
            //ObjectStatistics obj1 = new ObjectStatistics("F", 29.765957771107);
            //ObjectStatistics obj2 = new ObjectStatistics("G", 0);
            //ObjectStatistics obj3 = new ObjectStatistics("H", 32.807804682612);
            //ObjectStatistics obj4 = new ObjectStatistics("J", 196.45946739256);
            //ObjectStatistics obj5 = new ObjectStatistics("K", 0.19434030906893);

            //List<ObjectStatistics> lst = new List<ObjectStatistics>();
            //lst.Add(obj1);
            //lst.Add(obj2);
            //lst.Add(obj3);
            //lst.Add(obj4);
            //lst.Add(obj5);
            if (strDate == "")
            {
                strDate = DateTime.Now.ToShortDateString();
            }
            DateTime date = DateTime.Parse(strDate);
            StatisticsService ss = new StatisticsService();
            List<ObjectStatistics> lst = ss.StatisticsMonth(date);

            string strJson = convertToJsonString(lst);

            return strJson;
        }
        [Authorize(Roles = "Admin")]
        public string StatisticsYear(String strDate = null)
        {
            //ObjectStatistics obj1 = new ObjectStatistics("L", 29.765957771107);
            //ObjectStatistics obj2 = new ObjectStatistics("M", 0);
            //ObjectStatistics obj3 = new ObjectStatistics("N", 32.807804682612);
            //ObjectStatistics obj4 = new ObjectStatistics("P", 196.45946739256);
            //ObjectStatistics obj5 = new ObjectStatistics("o", 0.19434030906893);

            //List<ObjectStatistics> lst = new List<ObjectStatistics>();
            //lst.Add(obj1);
            //lst.Add(obj2);
            //lst.Add(obj3);
            //lst.Add(obj4);
            //lst.Add(obj5);
            if (strDate == "")
            {
                strDate = DateTime.Now.ToShortDateString();
            }
            DateTime date = DateTime.Parse(strDate);
            StatisticsService ss = new StatisticsService();
            List<ObjectStatistics> lst = ss.StatisticsYear(date);
            string strJson = convertToJsonString(lst);

            return strJson;
        }
        [Authorize(Roles = "Admin")]
        public string Statistics10TopDay(String strDate = null)
        {
            if (strDate == "")
                strDate = DateTime.Now.ToShortDateString();
            DateTime date = DateTime.Parse(strDate);
            StatisticsService ss = new StatisticsService();
            List<ProductStatistics> lst = ss.StatisticsTop10ProductDay(date);

            //ObjectStatistics obj1 = new ObjectStatistics("T", 29.765957771107);
            //ObjectStatistics obj2 = new ObjectStatistics("O", 0);
            //ObjectStatistics obj3 = new ObjectStatistics("P", 32.807804682612);
            //ObjectStatistics obj4 = new ObjectStatistics("D", 196.45946739256);
            //ObjectStatistics obj5 = new ObjectStatistics("A", 0.19434030906893);

            //List<ObjectStatistics> lst = new List<ObjectStatistics>();
            //lst.Add(obj1);
            //lst.Add(obj2);
            //lst.Add(obj3);
            //lst.Add(obj4);
            //lst.Add(obj5);
            if (strDate == null)
            {
                strDate = DateTime.Now.ToShortDateString();
            }

            string strJson = convertToJsonString(lst);
            return strJson;
        }
        [Authorize(Roles = "Admin")]
        public string Statistics10TopMonth(String strDate = null)
        {
            if (strDate == "")
                strDate = DateTime.Now.ToShortDateString();
            DateTime date = DateTime.Parse(strDate);
            StatisticsService ss = new StatisticsService();

            List<ProductStatistics> lst = ss.StatisticsTop10ProductMonth(date);

            //ObjectStatistics obj1 = new ObjectStatistics("T", 29.765957771107);
            //ObjectStatistics obj2 = new ObjectStatistics("A", 0);
            //ObjectStatistics obj3 = new ObjectStatistics("P", 32.807804682612);
            //ObjectStatistics obj4 = new ObjectStatistics("M", 196.45946739256);
            //ObjectStatistics obj5 = new ObjectStatistics("O", 0.19434030906893);

            //List<ObjectStatistics> lst = new List<ObjectStatistics>();
            //lst.Add(obj1);
            //lst.Add(obj2);
            //lst.Add(obj3);
            //lst.Add(obj4);
            //lst.Add(obj5);

            string strJson = convertToJsonString(lst);
            return strJson;
        }
        [Authorize(Roles = "Admin")]
        public string Statistics10TopYear(String strDate = null)
        {
            if (strDate == "")
                strDate = DateTime.Now.ToShortDateString();
            DateTime date = DateTime.Parse(strDate);
            StatisticsService ss = new StatisticsService();
            List<ProductStatistics> lst = ss.StatisticsTop10ProductYear(date);

            //ObjectStatistics obj1 = new ObjectStatistics("T", 29.765957771107);
            //ObjectStatistics obj2 = new ObjectStatistics("O", 0);
            //ObjectStatistics obj3 = new ObjectStatistics("P", 32.807804682612);
            //ObjectStatistics obj4 = new ObjectStatistics("Y", 196.45946739256);
            //ObjectStatistics obj5 = new ObjectStatistics("E", 0.19434030906893);

            //List<ObjectStatistics> lst = new List<ObjectStatistics>();
            //lst.Add(obj1);
            //lst.Add(obj2);
            //lst.Add(obj3);
            //lst.Add(obj4);
            //lst.Add(obj5);

            string strJson = convertToJsonString(lst);
            return strJson;
        }

        private string convertToJsonString(List<ObjectStatistics> lst)
        {
            string strJson = "[{ \"key\": \"Cumulative Return\", \"values\": [";
            StringBuilder strBuilder = new StringBuilder();
            foreach (var obj in lst)
            {
                strBuilder.Append("{");
                strBuilder.AppendFormat(" \"label\" : \"{0}\", \"value\" : {1}", obj.label, obj.value);
                strBuilder.Append("},");
            }

            if (lst.Count > 0)
                strBuilder.Remove(strBuilder.Length - 1, 1);

            strJson += strBuilder.ToString() + "] }]";
            return strJson;
        }

        private string convertToJsonString(List<ProductStatistics> lst)
        {
            string strJson = "[{ \"key\": \"Cumulative Return\", \"values\": [";
            StringBuilder strBuilder = new StringBuilder();
            foreach (var obj in lst)
            {
                strBuilder.Append("{");
                strBuilder.AppendFormat(" \"label\" : \"{0}\", \"value\" : {1}", obj.Name + ":" + obj.ID, obj.Count);
                strBuilder.Append("},");
            }
            if(lst.Count > 0)
                strBuilder.Remove(strBuilder.Length - 1, 1);

            strJson += strBuilder.ToString() + "] }]";
            return strJson;
        }
    }
}