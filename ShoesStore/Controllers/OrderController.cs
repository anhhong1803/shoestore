﻿using ShoesStore.Models;
using ShoesStore.Models.Services;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ShoesStore.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private ShoeStoreEntities db = new ShoeStoreEntities();

        [Authorize(Roles = "Admin")]
        public ActionResult Index(int? numPerPage, int? page, String orderByField)
        {

            bool descending = false;
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;
            
            if (orderByField == null)
            {
                orderByField = "IDOrder";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }
            ViewBag.orderByField = orderByField;
           

            // tìm danh sách Status
            //ViewBag.lstStatus = getAllStatus();
            OrderService orderService = new OrderService();


            List<OrderViewModel> lstOrderViewModel;

            lstOrderViewModel = orderService.GetAllOrder((int)numPerPage, (int)page, orderByField, descending);

            ViewBag.PageCount = (orderService.CountOrder() % numPerPage) == 0 ? orderService.CountOrder() / numPerPage : (orderService.CountOrder() / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumOrder = lstOrderViewModel.Count;

            return View(lstOrderViewModel);

        }

        // Support Method      
        private List<SelectListItem> getAllStatus()
        {
            OrderStatusService osv = new OrderStatusService();
   
            // tìm danh sách Status
            List<OrderStatusViewModel> lstStatus = osv.GetAllStatus();
            List<SelectListItem> lstSeStatus = new List<SelectListItem>();
            foreach (OrderStatusViewModel status in lstStatus)
            {

                lstSeStatus.Add(new SelectListItem { Text = status.OrderStatus, Value = status.ID.ToString() });
            }
            return lstSeStatus;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Index(int? numPerPage, int? page, String orderByField, int? status, int? idOrder)
        {
            OrderService orderService = new OrderService();
            if (idOrder.HasValue && status.HasValue)
            {
                // Cập nhật tình trạng đơn hàng
                ViewBag.notice = orderService.UpdateOrderStatus((int)idOrder, (int)status);
            }

            bool descending = false;
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;

            if (orderByField == null)
            {
                orderByField = "IDOrder";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }
            ViewBag.orderByField = orderByField;


            // tìm danh sách Status
            //ViewBag.lstStatus = getAllStatus();
          


            List<OrderViewModel> lstOrderViewModel;

            lstOrderViewModel = orderService.GetAllOrder((int)numPerPage, (int)page, orderByField, descending);

            ViewBag.PageCount = (orderService.CountOrder() % numPerPage) == 0 ? orderService.CountOrder() / numPerPage : (orderService.CountOrder() / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumOrder = lstOrderViewModel.Count;

            return View(lstOrderViewModel);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult OrderDetailAdmin(int? idOrder)
        {
            string notice = null;
            OrderService orderService = new OrderService();
            List<OrderDetailViewModel> lstOrderDetail = new List<OrderDetailViewModel>();
            OrderViewModel  order = new OrderViewModel();
            ProductService psv = new ProductService();

            if (idOrder.HasValue)
            {
                order = orderService.GetOrderByID((int)idOrder);
                if (order != null)
                {
                    OrderStatusService osv = new OrderStatusService();

                    ViewBag.IDOrder = idOrder;
                    ViewBag.UserName = order.UserName;
                    ViewBag.Date = order.Date;
                    ViewBag.Total = order.Total;
                    ViewBag.Status = osv.GetStatusByIdOrder((int)idOrder);

                    lstOrderDetail = orderService.GetOrderDetail((int)idOrder);
                    

                    for(int i = 0; i < lstOrderDetail.Count; i++)
                    {
                        ProductViewModel product = new ProductViewModel();
                        product = psv.GetProduct(lstOrderDetail[i].IDProduct);

                        lstOrderDetail[i].ProductName = product.ProductName;
                        lstOrderDetail[i].Image = product.Image;
                        lstOrderDetail[i].Size = psv.GetSize(product.IDSize);
                        lstOrderDetail[i].Color = psv.GetColor(product.IDColor);
                    }
                }
                
            }
            return View(lstOrderDetail);
        }

        [Authorize(Roles = "User, Admin, Mod")]
        public ActionResult HistoryOrder(int? numPerPage, int? page, String orderByField)
        {

            string idUser = User.Identity.GetUserId();
            bool descending = false;
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;

            if (orderByField == null)
            {
                orderByField = "IDOrder";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }
            ViewBag.orderByField = orderByField;
         
            OrderService orderService = new OrderService();
            OrderStatusService oStatusService = new OrderStatusService();

            

            List<OrderViewModel> lstOrderViewModel;

            lstOrderViewModel = orderService.GetAllOrderOfUser((int)numPerPage, (int)page, orderByField, idUser, descending);
            for (int i = 0; i < lstOrderViewModel.Count; i++ )
            {
                lstOrderViewModel[i].OrderStatus = oStatusService.GetStatusByIdOrder(lstOrderViewModel[i].ID);
            }

            ViewBag.PageCount = (orderService.CountOrderOfUser(idUser) % numPerPage) == 0 ? orderService.CountOrderOfUser(idUser) / numPerPage : (orderService.CountOrderOfUser(idUser) / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumOrder = lstOrderViewModel.Count;

            return View(lstOrderViewModel);
        }

        [Authorize(Roles = "User, Admin, Mod")]
        public ActionResult OrderDetailUser(int? idOrder)
        {
            string notice = null;
            OrderService orderService = new OrderService();
            List<OrderDetailViewModel> lstOrderDetail = new List<OrderDetailViewModel>();
            OrderViewModel order = new OrderViewModel();
            ProductService psv = new ProductService();

            if (idOrder.HasValue)
            {
                order = orderService.GetOrderByID((int)idOrder);
                if (order != null)
                {
                    OrderStatusService osv = new OrderStatusService();

                    ViewBag.IDOrder = idOrder;
                    ViewBag.UserName = order.UserName;
                    ViewBag.Date = order.Date;
                    ViewBag.Total = order.Total;
                    ViewBag.IDOrderStatus = order.IDOrderStatus;
                    ViewBag.Status = osv.GetStatusByIdOrder((int)idOrder);

                    lstOrderDetail = orderService.GetOrderDetail((int)idOrder);


                    for (int i = 0; i < lstOrderDetail.Count; i++)
                    {
                        ProductViewModel product = new ProductViewModel();
                        product = psv.GetProduct(lstOrderDetail[i].IDProduct);

                        lstOrderDetail[i].ProductName = product.ProductName;
                        lstOrderDetail[i].Image = product.Image;
                        lstOrderDetail[i].Size = psv.GetSize(product.IDSize);
                        lstOrderDetail[i].Color = psv.GetColor(product.IDColor);
                    }
                }

            }
            return View(lstOrderDetail);
        }

        [Authorize(Roles = "User, Admin, Mod")]
        public ActionResult DeleteOrderByUser(int? idOrder)
        {
            OrderService orderService = new OrderService();
            if (idOrder.HasValue)
            {
                // Cập nhật tình trạng đơn hàng
                ViewBag.notice = orderService.DeleteOrderByUser((int)idOrder);
            }
            return RedirectToAction("HistoryOrder", "Order");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult UnconfirmedOrder(int? numPerPage, int? page, String orderByField, int? orderStatus)
        {
            if (!orderStatus.HasValue)
            {
                //orderStatus = idOrderStatus;
                orderStatus = 1;
            }
             ViewBag.OrderStatus = orderStatus;
            switch (orderStatus)
            {
                case 1: ViewBag.Name = "Danh sách đơn hàng chưa xác nhận"; break;
                case 2: ViewBag.Name = "Danh sách đơn hàng đã xác nhận"; break;
                case 4: ViewBag.Name = "Danh sách đơn hàng đã giao"; break;
                default: break;
            }
            
            bool descending = false;
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;

            if (orderByField == null)
            {
                orderByField = "IDOrder";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }
            ViewBag.orderByField = orderByField;
            if (!orderStatus.HasValue)
            {
                orderStatus = 1;
            }

            // tìm danh sách Status
            //ViewBag.lstStatus = getAllStatus();
            OrderService orderService = new OrderService();
            OrderStatusService orderStatusService = new OrderStatusService();

            List<OrderViewModel> lstOrderViewModel;

            lstOrderViewModel = orderService.GetAllOrderByStatus((int)numPerPage, (int)page, orderByField, (int)orderStatus, descending);
            for (int i = 0; i < lstOrderViewModel.Count; i++ )
            {
                lstOrderViewModel[i].OrderStatus = orderStatusService.GetStatusByIdOrder(lstOrderViewModel[i].IDOrderStatus);
            }

            ViewBag.PageCount = (orderService.CountOrderByStatus((int)orderStatus) % numPerPage) == 0 ? orderService.CountOrderByStatus((int)orderStatus) / numPerPage : (orderService.CountOrderByStatus((int)orderStatus) / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumOrder = lstOrderViewModel.Count;

            return View(lstOrderViewModel);

        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult UnconfirmedOrder(int? numPerPage, int? page, String orderByField, int? orderStatus, int? idOrder, int? idOrderStatus)
        {
            OrderService orderService = new OrderService();
            OrderStatusService orderStatusService = new OrderStatusService();

            if (idOrder.HasValue && idOrderStatus.HasValue)
            {
                // Cập nhật tình trạng đơn hàng
                ViewBag.notice = orderService.UpdateOrderStatus((int)idOrder, (int)idOrderStatus);
            }

            if (!orderStatus.HasValue)
            {
                //orderStatus = idOrderStatus;
                orderStatus = 1;
            }
            ViewBag.OrderStatus = orderStatus;
            switch (orderStatus)
            {
                case 1: ViewBag.Name = "Danh sách đơn hàng chưa xác nhận"; break;
                case 2: ViewBag.Name = "Danh sách đơn hàng đã xác nhận"; break;
                case 4: ViewBag.Name = "Danh sách đơn hàng đã giao"; break;
                default: break;
            }

            bool descending = false;
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;

            if (orderByField == null)
            {
                orderByField = "IDOrder";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }
            ViewBag.orderByField = orderByField;
            if (!orderStatus.HasValue)
            {
                orderStatus = 1;
            }

            // tìm danh sách Status
            //ViewBag.lstStatus = getAllStatus();
            

            List<OrderViewModel> lstOrderViewModel;

            lstOrderViewModel = orderService.GetAllOrderByStatus((int)numPerPage, (int)page, orderByField, (int)orderStatus, descending);
            for (int i = 0; i < lstOrderViewModel.Count; i++)
            {
                lstOrderViewModel[i].OrderStatus = orderStatusService.GetStatusByIdOrder(lstOrderViewModel[i].IDOrderStatus);
            }

            ViewBag.PageCount = (orderService.CountOrderByStatus((int)orderStatus) % numPerPage) == 0 ? orderService.CountOrderByStatus((int)orderStatus) / numPerPage : (orderService.CountOrderByStatus((int)orderStatus) / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumOrder = lstOrderViewModel.Count;

            return View(lstOrderViewModel);
        }
    }
}