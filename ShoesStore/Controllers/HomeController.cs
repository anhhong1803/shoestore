﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoesStore.Models;
using ShoesStore.Models.Services;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.EF;

namespace ShoesStore.Controllers
{
    
    public class HomeController : Controller
    {
        private HomeService homeService = new HomeService();
        private ProductService productService = new ProductService();

       
        // GET: Home
        
        public ActionResult Index()
        {
      
            List<ProductViewModel> lstProduct = new List<ProductViewModel>();
            ViewBag.Count = lstProduct.Count;
            // Tìm danh sách sp có số view cao nhất

            lstProduct = productService.GetAllProductByView();

            return View(lstProduct);
        }


        
        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Search(int? idBrand, int? idSize, double? price, int? idColor,string name = null, int indPage = 1, int numProductPerPage = 6)
        {
            List<ProductViewModel> lstProduct = new List<ProductViewModel>();
            int numProduct = 0;

            ViewBag.Header = "TÌM KIẾM NÂNG CAO";
            ViewBag.lstBrand = getListBrand();
            ViewBag.lstSize = getListSize();
            ViewBag.lstPrice = getListPrice();
            ViewBag.lstColor = getListColor();
            ViewBag.numProductPerPage = numProductPerPage;

            if (name == null)
            {
                if (idBrand.HasValue && idSize.HasValue && price.HasValue && idColor.HasValue)
                {
                    lstProduct = homeService.AdSearch(ref numProduct, ViewBag.numProductPerPage, (int)idBrand, (int)idSize, (double)price, (int)idColor, indPage);
                }
            }
            else
            {
                lstProduct = homeService.Search(ref numProduct, ViewBag.numProductPerPage, name, indPage);
            }
 
            ViewBag.lstProduct = lstProduct;
            ViewBag.numProduct = numProduct;
            ViewBag.indPage = indPage;

            // Giữ giá trị biến để phân trang
            ViewBag.idBrand = idBrand;
            ViewBag.idSize = idSize;
            ViewBag.price = price;
            ViewBag.idColor = idColor;
            ViewBag.name = name;

            return View();
        }


        public ActionResult Notification(string Message)
        {
            ViewBag.Message = Message;
            return View();
        }


        // Support Method
        private List<SelectListItem> getListBrand()
        {
            // Gọi hàm lấy Brand từ ProductService
            List<Brand> lstAllBrand = productService.GetAllBrand();
            List<SelectListItem> lstBrand = new List<SelectListItem>();
            lstBrand.Add(new SelectListItem { Text = "Tất cả", Value = "0" });
            foreach(Brand brand in lstAllBrand)
            {
                lstBrand.Add(new SelectListItem { Text = brand.BrandName, Value = brand.ID.ToString() });
            }
            //SelectList lstBrand = new SelectList(lstAllBrand);
            return lstBrand;
        }
        private List<SelectListItem> getListPrice()
        {
            List<SelectListItem> lstAllPrice = new List<SelectListItem>();
            lstAllPrice.Add(new SelectListItem { Text = "Tất cả", Value = "0" });
            lstAllPrice.Add(new SelectListItem { Text = "500.000", Value = "500000" });
            lstAllPrice.Add(new SelectListItem { Text = "1.000.000", Value = "1000000" });
            lstAllPrice.Add(new SelectListItem { Text = "2.000.000", Value = "2000000" });
            lstAllPrice.Add(new SelectListItem { Text = "3.000.000", Value = "3000000" });
            lstAllPrice.Add(new SelectListItem { Text = "5.000.000", Value = "5000000" });
            return lstAllPrice;

            //List<String> lstAllPrice = new List<String>();
            //lstAllPrice.Add("100");
            //lstAllPrice.Add("200");

            //SelectList lstPrice = new SelectList(lstAllPrice);
            //return lstPrice;
        }
        private List<SelectListItem> getListSize()
        {
            List<Size> lstAllSize = productService.GetAllSize();
            List<SelectListItem> lstSize = new List<SelectListItem>();
            lstSize.Add(new SelectListItem { Text = "Tất cả", Value = "0" });
            foreach(Size size in lstAllSize)
            {
                lstSize.Add(new SelectListItem { Text = size.SizeName, Value = size.ID.ToString() });
            }
            //SelectList lstSize = new SelectList(lstAllSize);
            return lstSize;
        }

        private List<SelectListItem> getListColor()
        {
            List<Color> lstAllColor = productService.GetAllColor();

            List<SelectListItem> lstColor = new List<SelectListItem>();
            lstColor.Add(new SelectListItem { Text = "Tất cả", Value = "0" });
            foreach(Color color in lstAllColor)
            {
                lstColor.Add(new SelectListItem { Text = color.ColorName, Value = color.ID.ToString() });
            }

            //SelectList lstColor = new SelectList(lstAllColor);
            return lstColor;
        }
        //private int getNumProduct(int? idBrand, int? idSize, double? price, int? idColor, string name = null)
        //{
        //    if (name == null)
        //    {
        //        if (!idBrand.HasValue)
        //        {
        //            idBrand = 0;
        //        }
        //        if (!idSize.HasValue)
        //        {
        //            idSize = 0;
        //        }
        //        if (!price.HasValue)
        //        {
        //            price = 0;
        //        }
        //        if (!idColor.HasValue)
        //        {
        //            idColor = 0;
        //        }

        //        lstProduct = homeService.getNumProAdSearch((int)idBrand, (int)idSize, (double)price, (int)idColor, indPage);
        //    }
        //    else
        //    {
        //        lstProduct = homeService.getNumProSearch(name, indPage);
        //    }
        //}
    }
}