﻿using ShoesStore.Models;
using ShoesStore.Models.Services;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ShoesStore.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        private ShoeStoreEntities db = new ShoeStoreEntities();

        [Authorize(Roles = "Admin")]
        public ActionResult Index(int? numPerPage, int? page, String orderByField)
        {
            bool descending = false;
            if (numPerPage == null) { numPerPage = 10; }
            if (page == null)
            {
                page = 1;
            }
            ViewBag.STT = (page - 1) * numPerPage;

            if (orderByField == null)
            {
                orderByField = "CategoryName";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }
            ViewBag.orderByField = orderByField;

            CategoryService csv = new CategoryService();


            List<CategoryViewModel> lstCategory = new List<CategoryViewModel>();
            // Lấy tất cả danh mục theo phân trang
            lstCategory = csv.GetAllCategory((int)numPerPage, (int)page, orderByField, descending);
            // Lấy số lượng danh muc
            List<CategoryViewModel> lstCategory1 = csv.GetAllCategory();
            ViewBag.PageCount = (lstCategory1.Count % numPerPage) == 0 ? lstCategory1.Count / numPerPage : (lstCategory1.Count / numPerPage) + 1;
            ViewBag.CurrentPage = page;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumCategory = lstCategory.Count;

            return View(lstCategory);

            
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Add(AddCategoryViewModel category)
        {
           
            string notice = null;
            int i = category.ID;
            if (category.CategoryName != null)
            {
                CategoryService csv = new CategoryService();
                if (i != 0)
                {
                    ViewBag.SubTitle = "Cập nhật thông tin loại sản phẩm";
                    notice = csv.EditCategory(category.ID, category.CategoryName);
                }
                else
                {
                    ViewBag.SubTitle = "Thông tin loại sản phẩm";
                    notice = csv.AddCategory(category.CategoryName);
                }
               
                ViewBag.Notice = notice;
                return View();
            }
            else
            {
                
                return View(category);
            }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Add(int? idCategory)
        {
            if (idCategory.HasValue)
            {
                ViewBag.SubTitle = "Cập nhật thông tin loại sản phẩm";
                // Tìm category có id tương ứng
                CategoryService csv = new CategoryService();
                AddCategoryViewModel category = new AddCategoryViewModel();
                category = csv.FindCategoryById((int)idCategory);

                return View(category);
            }
            ViewBag.SubTitle = "Thông tin loại sản phẩm";
            return View();
        }

       

        // Xóa loại sản phẩm
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string idCategory)
        {
            int id = Int32.Parse(idCategory);
            CategoryService csv = new CategoryService();
            csv.DeleteCategory(id);
            return RedirectToAction("Index", "Category");
        }

    }
}