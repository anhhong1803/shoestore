﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Antlr.Runtime.Tree;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using ShoesStore.Models;
using ShoesStore.Models.Services;
using ShoesStore.Models.ViewModel;

namespace ShoesStore.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        AccountService mAccountService = new AccountService();
        private ApplicationSignInManager _signInManager; // để giành làm j m?, cái đó quản ly dang nhap, dang xuat ak
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;
        
        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return _roleManager ?? new ApplicationRoleManager(); }
            set { _roleManager = value; }
        }


        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl)
        {
           // var user = UserManager.GetLockoutEnabled(User.Identity.GetUserId());
            if (User.Identity.GetUserId() != null)
                return RedirectToAction("Index", "Home");
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string ReturnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // Require the user to have a confirmed email before they can log on.
            var user = await UserManager.FindByNameAsync(model.Username);
            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    ViewBag.Message = "Bạn phải kích hoạt tài khoản trước khi đăng nhập.";
                    return View("Notification");
                }

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, shouldLockout: false);

                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            //var user = await UserManager.FindByNameAsync(model.Username);
                            //var roles = UserManager.GetRoles(user.Id);

                            if (UserManager.IsInRole(user.Id, "Admin"))
                            {
                                return RedirectToAction("ShowAllAccounts", "Account");
                            }

                            return RedirectToLocal(ReturnUrl);
                        }

                    //case SignInStatus.LockedOut:
                    //    return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        //return RedirectToAction("ConfirmAccount", new { ReturnUrl = ReturnUrl, RememberMe = true });
                        break;
                    case SignInStatus.Failure:
                    default:
                        break;

                }
            }

            ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không đúng!");
            model.Password = null;
            return View(model);
        }

        //get:
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });

            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            // Chỉ xài mấy cái nó có sẵn trong UserMangaer đó à?
            //uhm t thay nhiu do cug đuỏi, xai của nó, nó quản lý cai logni dum minh lun ùm :| giờ phân mAccountController hay sao?
            //phân cái đso la sao ku, chớ giờ m tính làm sao?
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });

            if (ModelState.IsValid)
            {
                var response = Request["g-recaptcha-response"];
                //secret that was generated in key value pair
                string secret = ConfigurationManager.AppSettings["ReCapchar_SecretKey"];

                var client = new WebClient();
                var reply =
                    client.DownloadString(
                        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

                var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
                if (captchaResponse.Success)
                {
                    var user = new ApplicationUser
                    {
                        UserName = model.Username,
                        Email = model.Email,
                        Name = model.Name,
                        Address = model.Address,
                        Gender = model.Gender,
                        BirthDay = model.Birthday,
                        RegisteredDate = DateTime.Now,
                        PhoneNumber = model.Phone,
                        Status = 0
                    };
                    var result = await UserManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        result = await UserManager.AddToRoleAsync(user.Id, "User");
                        if (result.Succeeded)
                        {
                            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                            // Send an email with this link
                            await SendConfirmMail(user.Id);

                            ViewBag.Message = "Một đường dẫn kích hoạt đã được gửi đến email của bạn. Bạn cần kích hoạt tài khoản trước khi đăng nhâp!";
                            return View("Notification");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Hệ thống có lỗi, vui lòng thử lại!");
                            //AddErrors(result);
                        }
                    }
                    AddErrors(result);
                }
                else
                {
                    ModelState.AddModelError("", "Capcha không đúng, vui lòng thử lại.");
                }
            }

            model.Password = model.ConfirmPassword = null;
            return View(model);
        }

        //Trang Quên mật khẩu
        [AllowAnonymous]
        public ActionResult ForgorPassword()
        {
            return View();
        }

        //Xử request quên mật khẩu
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgorPassword(ForgorPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                string accountSign = model.Username;

                var user = await UserManager.FindByNameAsync(accountSign);
                if (user != null && (await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    await SendResetPasswordMail(user.Id);

                    ViewBag.Message = "Hướng dẫn lấy lại mật khẩu đã được gủi đến email của bạn!";
                    return View("Notification");
                }
                else
                {
                    user = await UserManager.FindByEmailAsync(accountSign);
                    if (user != null && (await UserManager.IsEmailConfirmedAsync(user.Id)))
                    {
                        await SendResetPasswordMail(user.Id);

                        ViewBag.Message = "Hướng dẫn lấy lại mật khẩu đã được gủi đến email của bạn!";
                        return View("Notification");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Tài khoản không tồn tại.");
                        return View(model);
                    }
                }
            }

            return View(model);

        }

        private async Task SendResetPasswordMail(string userId)
        {
            string code = await UserManager.GeneratePasswordResetTokenAsync(userId);
            if (Request.Url != null)
            {
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = userId, code = code },
                    protocol: Request.Url.Scheme);
                await
                    UserManager.SendEmailAsync(userId, "Đặt Lại Mật Khẩu",
                        "Bấm vào <a href=\"" + callbackUrl + "\">đây</a> để đặt lại mật khẩu.<br>Link này chỉ có hiệu lực trong vòng 24 giờ!");
            }
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string code)
        {
            if (userId==null || code==null)
            {
                return RedirectToAction("Index", "Home");
            }

            ResetPasswordViewModel model = new ResetPasswordViewModel
            {
                UserId = userId,
                Token = code
            };
            return View(model);
        }


        //
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    IdentityResult result = await UserManager.ResetPasswordAsync(model.UserId, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        ViewBag.Message = "Đặt lại mật khẩu mới thành công!";
                    }

                    // If we got this far, something failed.
                    AddErrors(result);
                    foreach (var error in result.Errors)
                    {
                        ViewBag.Message += "\n" + error;
                    }
                    return View("Notification");
                }
                catch (InvalidOperationException ioe)
                {
                    // ConfirmEmailAsync throws when the userId is not found.
                    ViewBag.Message = ioe.Message;
                    return View("Notification");
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmAccount(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction("Index", "Home");
            }
            try
            {
                IdentityResult result = await UserManager.ConfirmEmailAsync(userId, code);
                if (result.Succeeded)
                {
                    return View();
                }

                // If we got this far, something failed.
                AddErrors(result);
                foreach (var error in result.Errors)
                {
                    ViewBag.Message +="\n"+ error;
                }
                return View("Notification");
            }
            catch (InvalidOperationException ioe)
            {
                // ConfirmEmailAsync throws when the userId is not found.
                ViewBag.Message = ioe.Message;
                return View("Notification");
            }
        }

        [AllowAnonymous]
        public ActionResult ResendConfirmMail()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResendConfirmMail(ResendConfirmMailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Username);
                if (user != null)
                {
                    if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                    {
                        await SendConfirmMail(user.Id);

                        ViewBag.Message =
                            "Một đường dẫn kích hoạt đã được gửi đến email của bạn.";
                        return View("Notification");
                    }
                    else
                    {
                        ViewBag.Message = "Tài khoản của bạn đã được kích hoạt!";
                        return View("Notification");
                    }
                }
                else
                {
                    ViewBag.Message = "Tài khoản này không tồn tại, vui lòng kiểm tra lại!";
                    return View("Notification");
                }
            }

            return View(model);
        }

        private async Task SendConfirmMail(string userId)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userId);
            if (Request.Url != null)
            {
                var callbackUrl = Url.Action("ConfirmAccount", "Account",
                    new { userId = userId, code = code }, protocol: Request.Url.Scheme);
                await
                    UserManager.SendEmailAsync(userId, "Kích hoạt tài khoản",
                        "Nhấn vào <a href=\"" + callbackUrl + "\">đây</a> để kích hoạt tài khoản!<br>"+"Link kích hoạt chỉ có hiệu lực trong vòng 24h.<br>");
            }
        }

        [Authorize]
        public ActionResult EditProfile()
        {
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });
            // mấy đó là sao :|, mấy nào ba, cái đó là cái hàm tìm cái user theo id ak
            // hình như có 2 cái
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            EditProfileViewModel model = new EditProfileViewModel
            {
                Name = user.Name,
                Email = user.Email,
                Address = user.Address,
                Gender = user.Gender,
                Birthday = user.BirthDay,
                Phone = user.PhoneNumber
            };


            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProfile(EditProfileViewModel model)
        {
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });

            if (ModelState.IsValid)
            {
                ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
                //UserName = model.Username,
                //user.Email = model.Email;
                user.Name = model.Name;
                user.Address = model.Address;
                user.Gender = model.Gender;
                user.BirthDay = model.Birthday;
                user.PhoneNumber = model.Phone;
                var result = await UserManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    model.Email = user.Email;
                    ViewBag.Message = "Cập Nhật Thành Công!";
                    ModelState.AddModelError("", "Cập Nhật Thành Công!");
                }
                else
                {
                    model.Email = user.Email;
                     AddErrors(result);
                }
            }

            //model.Email = user.Email;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/ShowAllAccounts
        [Authorize(Roles = "Admin")]
        public ActionResult ShowAllAccounts(int? numPerPage, int? pageNum, String orderByField)
        {
            bool descending = false;

            if (numPerPage == null) { numPerPage = 10; }
            if (pageNum == null)
            {
                pageNum = 1;
            }


            if (orderByField == null)
            {
                orderByField = "User";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }

            List<AccountViewModel> lstAccountViewModel;

            lstAccountViewModel = mAccountService.GetAllAccount((int)numPerPage, (int)pageNum, orderByField, descending);
            lstAccountViewModel.Remove(lstAccountViewModel.FirstOrDefault(i => i.Username == User.Identity.GetUserName()));

            ViewBag.PageCount = (lstAccountViewModel.Count % numPerPage) == 0 ? lstAccountViewModel.Count / numPerPage : (lstAccountViewModel.Count / numPerPage) + 1;
            ViewBag.CurrentPage = pageNum;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumAcc = lstAccountViewModel.Count;
            return View(lstAccountViewModel);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult RoleUser(string username)
        {
            if (username == null)
                return RedirectToAction("ShowAllAccounts", "Account");

            ApplicationUser user = UserManager.FindByName(username);
            
            List<RoleViewModel> allRoles = mAccountService.GetAllRoles();
            
           

            foreach (var role in user.Roles)
            {
                allRoles.Find(x => x.ID == role.RoleId).HasRole = true;
            }

            EditProfileViewModel model = new EditProfileViewModel
            {
                Name = user.Name,
                Email = user.Email,
                Address = user.Address,
                Gender = user.Gender,
                Birthday = user.BirthDay,
                Phone = user.PhoneNumber,
                ListRoleViewModel = allRoles
            };

            ViewBag.username = username;
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });
            //ViewBag.lstRolesUser = allRoles;

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RoleUser(EditProfileViewModel editProfileViewModels, string username)
        {
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });
            if(ModelState.IsValid)
            {
                List<RoleViewModel> allRoles = mAccountService.GetAllRoles();

                ApplicationUser user = UserManager.FindByName(username);
                foreach (var role in user.Roles)
                {
                    allRoles.Find(x => x.ID == role.RoleId).HasRole = true;
                }


                List<string> nameRoles = new List<string>();
                List<string> nameUnRoles = new List<string>();
                for (var i = 0; i < editProfileViewModels.ListRoleViewModel.Count; i++)
                {
                    if (editProfileViewModels.ListRoleViewModel[i].HasRole && !allRoles[i].HasRole)
                        nameRoles.Add(editProfileViewModels.ListRoleViewModel[i].NameRole);
                    else if (!editProfileViewModels.ListRoleViewModel[i].HasRole && allRoles[i].HasRole)
                        nameUnRoles.Add(editProfileViewModels.ListRoleViewModel[i].NameRole);
                }
                // Phan quyền lại
                var res1 = await UserManager.AddToRolesAsync(user.Id, nameRoles.ToArray());
                var res2 = await UserManager.RemoveFromRolesAsync(user.Id, nameUnRoles.ToArray());
                if (res1.Succeeded && res2.Succeeded)
                {
                    ModelState.AddModelError("", "Phân quyền thành công");
                }
                else
                {
                    if (!res1.Succeeded)
                        AddErrors(res1);
                    if (!res2.Succeeded)
                        AddErrors(res2);
                }

                // Cập nhật thông tin
                //user = UserManager.FindById(User.Identity.GetUserId());
                //UserName = model.Username,
                //user.Email = model.Email;
                user.Name = editProfileViewModels.Name;
                user.Address = editProfileViewModels.Address;
                user.Gender = editProfileViewModels.Gender;
                user.BirthDay = editProfileViewModels.Birthday;
                user.PhoneNumber = editProfileViewModels.Phone;

                var result = await UserManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    ModelState.AddModelError("", "Cập Nhật Thành Công!");
                }
                else
                {
                    AddErrors(result);
                }

                ViewBag.username = username;
            }

            
            return View(editProfileViewModels);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(String username)
        {
            mAccountService.Delete(username);

            return RedirectToAction("ShowAllAccounts", "Account");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddUser()
        {
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });

            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> AddUser(AccountViewModel accountViewModel)
        {
            ViewBag.DSGioiTinh = new SelectList(new List<String>() { "Nam", "Nữ" });

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = accountViewModel.Username,
                    Email = accountViewModel.Email,
                    Name = accountViewModel.Name,
                    Address = accountViewModel.Address,
                    Gender = accountViewModel.Gender,
                    BirthDay = accountViewModel.Birthday,
                    RegisteredDate = DateTime.Now,
                    PhoneNumber = accountViewModel.Phone,
                    Status = 0
                };
                var result = await UserManager.CreateAsync(user, accountViewModel.Password);
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("RoleUser", "Account", new { username = user.UserName});
                }
                AddErrors(result);

                //String err = mAccountService.Register(accountViewModel);
                ////Neu Thanh cong hoặc tai khoan chưa kích hoạt
                //if (System.String.Compare(err, ShoesStoreBLL.ErrorCodeDetail.Reg_Success, System.StringComparison.Ordinal)==0
                //    || System.String.Compare(err, ShoesStoreBLL.ErrorCodeDetail.Reg_UnconfirmedAccount, System.StringComparison.Ordinal)==0)
                //{
                //    accountViewModel.Password = accountViewModel.ConfirmPassword = null;
                //    TempData["account"] = accountViewModel;
                //    return RedirectToAction("ConfirmAccount", "Account");
                //}
                //else
                //{
                //    ViewBag.Error = err;
                //    return View(accountViewModel);
                //}
            }

            return View(accountViewModel);
        }


        [Authorize(Roles = "Admin")]
        public ActionResult Search(String keyword, String searchfor, int? numPerPage, int? pageNum, String orderByField)
        {
            bool descending = false;
            if (searchfor == null)
            {
                searchfor = "All";
            }
            if (numPerPage == null) { numPerPage = 10; }
            if (pageNum == null)
            {
                pageNum = 1;
            }

            //Nhớ để hiển thị lại trag kqua
            ViewBag.KeyWord = keyword;
            ViewBag.SearchFor = searchfor;

            if (orderByField == null)
            {
                orderByField = "User";
            }
            else
            {
                if (TempData[orderByField] == null)
                {
                    TempData[orderByField] = false;
                }
                else
                {
                    TempData[orderByField] = !(bool)TempData[orderByField];
                    descending = (bool)TempData[orderByField];
                }
            }

            List<AccountViewModel> lstAccountViewModel;

            lstAccountViewModel = mAccountService.Search(keyword, searchfor, (int)numPerPage, (int)pageNum, orderByField, descending);
            lstAccountViewModel.Remove(lstAccountViewModel.FirstOrDefault(i => i.Username == User.Identity.GetUserName()));

            ViewBag.PageCount = (lstAccountViewModel.Count % numPerPage) == 0 ? lstAccountViewModel.Count / numPerPage : (lstAccountViewModel.Count / numPerPage) + 1;
            ViewBag.CurrentPage = pageNum;
            ViewBag.NumPerPage = numPerPage;
            ViewBag.NumAcc = lstAccountViewModel.Count;
            return View(lstAccountViewModel);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }

                if (_roleManager != null)
                {
                    _roleManager.Dispose();
                    _roleManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }

    public class CaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }
}