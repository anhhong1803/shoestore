﻿using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.Services
{
    public class CategoryService
    {
        private ShoesStoreBLL.BusServices.CategoryBus mCategoryBus;

        public CategoryService()
        {
            this.mCategoryBus = new CategoryBus();
        }

        public List<CategoryViewModel> GetListCategory()
        {
            List<CategoryViewModel> lstCategory = new List<CategoryViewModel>();

            List<Category> lstCategoryPoco = new List<Category>();
            lstCategoryPoco = mCategoryBus.GetListCategory();

            for (int i = 0; i < lstCategoryPoco.Count; i++)
            {
                CategoryViewModel category = new CategoryViewModel();
                category.ID = lstCategoryPoco[i].ID;
                category.CategoryName = lstCategoryPoco[i].CategoryName;
                category.Status = lstCategoryPoco[i].Status;

                lstCategory.Add(category);            
            }

            return lstCategory;
        }

        internal List<CategoryViewModel> GetAllCategory(int numPerPage, int page, string orderByField, bool descending = false)
        {
            List<CategoryViewModel> lstCategory = new List<CategoryViewModel>();

            List<Category> lstCategoryPoco = new List<Category>();
            lstCategoryPoco = mCategoryBus.GetAllCategory(numPerPage, page, orderByField, descending);

            for (int i = 0; i < lstCategoryPoco.Count; i++)
            {
                CategoryViewModel category = new CategoryViewModel();
                category.ID = lstCategoryPoco[i].ID;
                category.CategoryName = lstCategoryPoco[i].CategoryName;
                category.Status = lstCategoryPoco[i].Status;
                lstCategory.Add(category);
            }

            return lstCategory;
        }

        internal string AddCategory(string categoryName)
        {
            return mCategoryBus.AddCategory(categoryName);
        }


        // Lấy danh sách loại sp
        internal List<CategoryViewModel> GetAllCategory()
        {
            List<CategoryViewModel> lstCategory = new List<CategoryViewModel>();

            List<Category> lstCategoryPoco = new List<Category>();
            lstCategoryPoco = mCategoryBus.GetAllCategory();

            for (int i = 0; i < lstCategoryPoco.Count; i++)
            {
                CategoryViewModel category = new CategoryViewModel();
                category.ID = lstCategoryPoco[i].ID;
                category.CategoryName = lstCategoryPoco[i].CategoryName;
                category.Status = lstCategoryPoco[i].Status;
                lstCategory.Add(category);
            }

            return lstCategory;
        }

      
        // Danh sách thương hiệu
        internal List<BrandViewModel> GetAllBrand()
        {
            List<BrandViewModel> lstBrand = new List<BrandViewModel>();

            List<Brand> lstBrandPoco = new List<Brand>();
            lstBrandPoco = mCategoryBus.GetAllBrand();

            for (int i = 0; i < lstBrandPoco.Count; i++)
            {
                BrandViewModel brand = new BrandViewModel();
                brand.ID = lstBrandPoco[i].ID;
                brand.BrandName = lstBrandPoco[i].BrandName;
                lstBrand.Add(brand);
            }

            return lstBrand;
        }

        // Danh sách màu
        internal List<ColorViewModel> GetAllColor()
        {
            List<ColorViewModel> lstColor = new List<ColorViewModel>();

            List<Color> lstColorPoco = new List<Color>();
            lstColorPoco = mCategoryBus.GetAllColor();

            for (int i = 0; i < lstColorPoco.Count; i++)
            {
                ColorViewModel color = new ColorViewModel();
                color.ID = lstColorPoco[i].ID;
                color.ColorName = lstColorPoco[i].ColorName;
                lstColor.Add(color);
            }

            return lstColor;
        }

        // Danh sách size
        internal List<SizeViewModel> GetAllSize()
        {
            List<SizeViewModel> lstSize = new List<SizeViewModel>();

            List<Size> lstSizePoco = new List<Size>();
            lstSizePoco = mCategoryBus.GetAllSize();

            for (int i = 0; i < lstSizePoco.Count; i++)
            {
                SizeViewModel size = new SizeViewModel();
                size.ID = lstSizePoco[i].ID;
                size.SizeName = lstSizePoco[i].SizeName;
                lstSize.Add(size);
            }

            return lstSize;
        }

        internal int DeleteCategory(int id)
        {
            return mCategoryBus.DeleteCategory(id);
        }

        internal string EditCategory(int idCategory, string newCategoryName)
        {
            return mCategoryBus.EditCategory(idCategory, newCategoryName);
        }

        internal AddCategoryViewModel FindCategoryById(int idCategory)
        {
            AddCategoryViewModel category = new AddCategoryViewModel();

            Category categoryPoco = new Category();
            categoryPoco = mCategoryBus.FindCategoryById(idCategory);

            category.ID = categoryPoco.ID;
            category.CategoryName = categoryPoco.CategoryName;
            category.Status = categoryPoco.Status;

            return category;
        }
    }
}