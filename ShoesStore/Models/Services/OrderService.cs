﻿using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.Services
{
    public class OrderService
    {
        private OrderBus mOrderBus;

        public OrderService()
        {
            this.mOrderBus = new OrderBus();
        }

        // Lấy tất cả đơn hàng có phân trang , sắp xếp
        public List<OrderViewModel> GetAllOrder(int numPerPage, int pageNum, string orderByField, bool descending = false)
        {
            List<Order> lstOrder = mOrderBus.GetAllOrder(numPerPage, pageNum, orderByField, descending);

            List<OrderViewModel> lstOrderViewModel = new List<OrderViewModel>();
            foreach (var order in lstOrder)
            {
                OrderViewModel newOrder = new OrderViewModel
                {
                    ID = order.ID,
                    IDUser = order.IDUser,
                    IDOrderStatus = order.IDOrderStatus,
                    Date = order.Date,
                    Total = order.Total,
                    Status = order.Status,
                    UserName = order.User.Name
                   
                };
                lstOrderViewModel.Add(newOrder);
            }

            return lstOrderViewModel;
        }

        // Đếm số lượng đơn hàng
        public int CountOrder()
        {
            return mOrderBus.CountOrder();
        }

        // Cập nhật trạng thái Đơn hàng
        public string UpdateOrderStatus(int idOrder, int status)
        {
            return mOrderBus.UpdateOrderStatus(idOrder, status);
        }

        public List<OrderDetailViewModel> GetOrderDetail(int idOrder)
        {
            List<OrderDetailViewModel> result = new List<OrderDetailViewModel>();

            List<OrderDetail> lstOrderDetail = new List<OrderDetail>();
            lstOrderDetail = mOrderBus.GetOrderDetail(idOrder);
            foreach (var order in lstOrderDetail)
            {
                OrderDetailViewModel newOrder = new OrderDetailViewModel
                {
                    ID = order.ID,
                    IDOrder = order.IDOrder,
                    IDProduct = order.IDProduct,
                    Price = order.Price,
                    Quantity = order.Quantity
                };
                result.Add(newOrder);
            }
            return result;
        }

        public OrderViewModel GetOrderByID(int idOrder)
        {           
            Order order = new Order();
            order = mOrderBus.GetOrderByID(idOrder);
            OrderViewModel result = new OrderViewModel
            {
                ID = order.ID,
                IDUser = order.IDUser,
                IDOrderStatus = order.IDOrderStatus,
                Date = order.Date,
                Total = order.Total,
                Status = order.Status,
                UserName = order.User.Name

            };
            return result;
        }

        // Lấy tất cả đơn hàng có phân trang , sắp xếp của khách hàng
        public List<OrderViewModel> GetAllOrderOfUser(int numPerPage, int pageNum, string orderByField, string idUser, bool descending = false)
        {
            List<Order> lstOrder = mOrderBus.GetAllOrderOfUser(numPerPage, pageNum, orderByField, descending, idUser);

            List<OrderViewModel> lstOrderViewModel = new List<OrderViewModel>();
            foreach (var order in lstOrder)
            {
                OrderViewModel newOrder = new OrderViewModel
                {
                    ID = order.ID,
                    IDUser = order.IDUser,
                    IDOrderStatus = order.IDOrderStatus,
                    Date = order.Date,
                    Total = order.Total,
                    Status = order.Status,
                    UserName = order.User.Name
                   

                };
                lstOrderViewModel.Add(newOrder);
            }

            return lstOrderViewModel;
        }

        public int CountOrderOfUser(string idUser)
        {
            return mOrderBus.CountOrderOfUser(idUser);
        }

        // Hủy đơn hàng bởi User
        public string DeleteOrderByUser(int idOrder)
        {
             return mOrderBus.DeleteOrderByUser(idOrder);
        }

        // Lấy tất cả đơn hàng theo tình trạng có phân trang , sắp xếp 
        public List<OrderViewModel> GetAllOrderByStatus(int numPerPage, int pageNum, string orderByField, int orderStatus, bool descending = false)
        {
            List<Order> lstOrder = mOrderBus.GetAllOrderByStatus(numPerPage, pageNum, orderByField, orderStatus,descending);

            List<OrderViewModel> lstOrderViewModel = new List<OrderViewModel>();
            foreach (var order in lstOrder)
            {
                OrderViewModel newOrder = new OrderViewModel
                {
                    ID = order.ID,
                    IDUser = order.IDUser,
                    IDOrderStatus = order.IDOrderStatus,
                    Date = order.Date,
                    Total = order.Total,
                    Status = order.Status,
                    UserName = order.User.Name

                };
                lstOrderViewModel.Add(newOrder);
            }

            return lstOrderViewModel;
        }

        // Đếm số lượng đơn hàng theo tình trạng
        public int CountOrderByStatus(int orderStatus)
        {
            return mOrderBus.CountOrderByStatus(orderStatus);
        }
    }
}