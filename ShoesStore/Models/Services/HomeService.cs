﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;

namespace ShoesStore.Models.Services
{
    public class HomeService
    {
        private HomeBus mHomeBus;

        public HomeService()
        {
            this.mHomeBus = new HomeBus();
        }

        // Action Search
        public List<ProductViewModel> AdSearch(ref int numProduct, int numProductPerPage,  int brand, int size, double price, int color, int indPage)
        {
            // Nhận kết quả trả về, Chuyển sang ProductView

            List<Product> lstProductPoco = mHomeBus.AdSearch(ref numProduct, numProductPerPage, brand, size, price, color, indPage);
            List<ProductViewModel> lstProductView = new List<ProductViewModel>();

            foreach (Product product in lstProductPoco)
            {
                ProductViewModel productViewTemp = toProductView(product);
                lstProductView.Add(productViewTemp);
            }
            return lstProductView;
        }

        public List<ProductViewModel> Search(ref int numProduct, int numProductPerPage, string name, int indPage)
        {
            List<Product> lstProduct = mHomeBus.Search(ref numProduct, numProductPerPage, name, indPage);
            List<ProductViewModel> lstProductView = new List<ProductViewModel>();

            foreach (Product product in lstProduct)
            {
                ProductViewModel productView = toProductView(product);
                lstProductView.Add(productView);
            }
            return lstProductView;
        }

        //public int getNumProAdSearch()
        //{

        //}

        //public int getgetNumProSearch()
        //{

        //}
        // Support Method

        private ProductViewModel toProductView(Product productPoco)
        {
            ProductViewModel productView = new ProductViewModel();
            productView.ID = productPoco.ID;
            productView.ProductName = productPoco.ProductName;
            productView.IDCategory = productPoco.IDCategory;
            productView.IDSize = productPoco.IDSize;
            productView.IDColor = productPoco.IDColor;
            productView.IDBrand = productPoco.IDBrand;
            productView.SaleOff = productPoco.SaleOff;
            productView.Price = productPoco.Price;
            productView.Image = productPoco.Image;
            productView.Quantity = productPoco.Quantity;
            productView.Describe = productPoco.Describe;
            productView.NewProduct = productPoco.NewProduct;
            productView.Viewed = productPoco.Viewed;
            productView.LargeImage = productPoco.LargeImage;
            return productView;
        }
    }
}