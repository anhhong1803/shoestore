﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;

namespace ShoesStore.Models.Services
{
    public class CartService
    {

        string ShoppingCartId { get; set; }
        private ShoesStoreBLL.BusServices.CartBus mCartBus;

        public CartService()
        {
            this.mCartBus = new CartBus();
        }

        public const string CartSessionKey = "CartId";

        public static CartService GetCart(HttpContextBase context)
        {
            var cart = new CartService();
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }

        //// Helper method to simplify shopping cart calls
        public static CartService GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }

        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context != null && context.Session != null && context.Session[CartSessionKey] == null)
            {
                    // Generate a new random GUID using System.Guid class
                    Guid tempCartId = Guid.NewGuid();

                    // Send tempCartId back to client as a cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
            }

            if (context.Session != null)
            {
                return context.Session[CartSessionKey].ToString();
            }

            return "";
        }

        public int AddToCart(int procId, double price,  int quantity, int sizeId, int colorId)
        {
            return mCartBus.AddToCart(ShoppingCartId, procId, price, quantity, sizeId, colorId);
        }

        public int RemoveFromCart(int? procId)
        {
            return mCartBus.RemoveFromCart(ShoppingCartId, procId);
        }
        
        public void EmptyCart()
        {
            mCartBus.EmptyCart(ShoppingCartId);
        }

        public Order CreateOrder(string userId, CheckOutViewModel model=null)
        {
            Recipient rep = null;
            if (model != null)
            {
                rep= new Recipient()
                {
                    Name = model.Name,
                    Address = model.Address,
                    Email = model.Email,
                    PhoneNumber = model.Phone
                };
            }
                
            return mCartBus.CreateOrder(ShoppingCartId, userId, rep);
        }

        public int ChangeQuantity(int itemId, int quantity)
        {
            return mCartBus.ChangeQuantity(ShoppingCartId, itemId, quantity);
        }

        public List<CartItem> GetCartItems()
        {
            return mCartBus.GetCartItems(ShoppingCartId);
        }

        public int GetTotalQuantity()
        {
            List<CartItem> lst = mCartBus.GetCartItems(ShoppingCartId);
            int totalQuantity = 0;
            if (lst != null)
            {
                totalQuantity += lst.Sum(item => item.Quanity);
            }

            return totalQuantity;
        }

        public double GetTotalPrice()
        {
            List<CartItem> lst = mCartBus.GetCartItems(ShoppingCartId);
            double totalPrice = 0;
            if (lst != null)
            {
                totalPrice += lst.Sum(item => (item.Price*item.Quanity));
            }

            return totalPrice;
        }


    }
}