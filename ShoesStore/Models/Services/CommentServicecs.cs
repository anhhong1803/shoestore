﻿using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.Services
{
    public class CommentServicecs
    {
        private CommentBus mCommentBus;

        public CommentServicecs()
        {
            mCommentBus = new CommentBus();
        }

        private CommentViewModel convertToViewModel(Comment comment)
        {
            CommentViewModel commentViewModel = new CommentViewModel();

            commentViewModel.ID = comment.ID;
            commentViewModel.Name = comment.Name;
            commentViewModel.Comment1 = comment.Comment1;
            commentViewModel.IDProduct = comment.IDProduct;
            commentViewModel.Date = comment.Date;
            commentViewModel.Status = comment.Status;

            return commentViewModel;
        }

        private Comment convertToPOCO(CommentViewModel commentViewModel)
        {
            Comment comment = new Comment();

            comment.ID = commentViewModel.ID;
            comment.Name = commentViewModel.Name;
            comment.Comment1 = commentViewModel.Comment1;
            comment.IDProduct = commentViewModel.IDProduct;
            comment.Date = commentViewModel.Date;
            comment.Status = commentViewModel.Status;

            return comment;
        }

        public string AddComment(CommentViewModel commentViewModel)
        {
            Comment comment = convertToPOCO(commentViewModel);

            return mCommentBus.AddComment(comment);
        }

        public List<CommentViewModel> GetAllComment(int idProduct)
        {
            List<CommentViewModel> lstComment = new List<CommentViewModel>();

            List<Comment> lstCommentPOCO = mCommentBus.GetAllComment(idProduct);
            foreach(var comment in lstCommentPOCO)
            {
                lstComment.Add(convertToViewModel(comment));
            }

            return lstComment;
        }
    }
}