﻿using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.Services
{
    public class OrderStatusService
    {
        private OrderStatusBus mOrderStatusBus;

        public OrderStatusService()
        {
            this.mOrderStatusBus = new OrderStatusBus();
        }

        public OrderStatusViewModel ConvertToViewModel(OrderStatu s)
        {
            OrderStatusViewModel temp = new OrderStatusViewModel();
            temp.ID = s.ID;
            temp.OrderStatus = s.OrderStatus;
            
            return temp;
        }

        public List<OrderStatusViewModel> GetAllStatus()
        {
            List<OrderStatusViewModel> result = new List<OrderStatusViewModel>();

            List<OrderStatu> lstOrderStatus = new List<OrderStatu>();
            lstOrderStatus = mOrderStatusBus.GetAllStatus();
            foreach (OrderStatu s in lstOrderStatus)
            {
                OrderStatusViewModel temp = new OrderStatusViewModel();
                temp = ConvertToViewModel(s);

                result.Add(temp);
            }

            return result;
        }

        public string GetStatusByIdOrder(int idOrder)
        {
            return mOrderStatusBus.GetStatusByIdOrder(idOrder);
        }
    }
}