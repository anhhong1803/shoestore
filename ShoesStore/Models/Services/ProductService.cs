﻿using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.Services
{
    public class ProductService
    {
        private ProductBus mProductBus;

        public ProductService()
        {
            this.mProductBus = new ProductBus();
        }

        public ProductViewModel ConvertToViewModel(Product p)
        {
            ProductViewModel temp = new ProductViewModel();
            temp.ID = p.ID;
            temp.ProductName = p.ProductName;
            temp.IDCategory = p.IDCategory;
            temp.IDSize = p.IDSize;
            temp.IDColor = p.IDColor;
            temp.IDBrand = p.IDBrand;
            temp.SaleOff = p.SaleOff;
            temp.Price = p.Price;
            temp.Image = p.Image;
            temp.Quantity = p.Quantity;
            temp.Describe = p.Describe;
            temp.NewProduct = p.NewProduct;
            temp.Viewed = p.Viewed;
            temp.LargeImage = p.LargeImage;
            temp.MenWomen = p.MenWomen;
            temp.Status = p.Status;
            return temp;
        }

        public ViewProductViewModel ConvertToViewProductViewModel(ProductInfoPoco p)
        {
            ViewProductViewModel temp = new ViewProductViewModel();
            temp.ID = p.ID;
            temp.ProductName = p.ProductName;
            temp.IDCategory = p.IDCategory;
            temp.CategoryName = p.CategoryName;
            temp.IDSize = p.IDSize;
            temp.SizeName = p.SizeName;
            temp.IDColor = p.IDColor;
            temp.ColorName = p.ColorName;
            temp.IDBrand = p.IDBrand;
            temp.BrandName = p.BrandName;
            temp.SaleOff = p.SaleOff;
            temp.Price = p.Price;
            temp.Image = p.Image;
            temp.Quantity = p.Quantity;
            temp.Describe = p.Describe;
            temp.NewProduct = p.NewProduct;
            temp.Viewed = p.Viewed;
            temp.LargeImage = p.LargeImage;
            temp.Status = p.Status;
            temp.MenWomen = p.MenWomen;
            return temp;
        }

        // lấy danh sách sp  theo danh mục có phân trang, sắp xếp
        public List<ProductViewModel> GetAllProductByCategory(int idCategory, int idMenu, int productPage, int page, int arrange)
        {

            List<ProductViewModel> result = new List<ProductViewModel>();

            List<Product> lst_Products = mProductBus.GetAllProductByCategory(idCategory, idMenu, productPage, page, arrange);
            foreach (Product p in lst_Products)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
        }

        internal ProductViewModel GetProduct(int idProduct)
        {
            ProductViewModel result = new ProductViewModel();

            Product product = mProductBus.GetProduct(idProduct);
            result = ConvertToViewModel(product);

            return result;
        }

        // Lấy danh sách sp theo danh mục
        internal List<ProductViewModel> GetAllProductByCategory(int idCategory, int idMenu)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            List<Product> lst_Products = mProductBus.GetAllProductByCategory(idCategory, idMenu);
            foreach (Product p in lst_Products)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
            
        }

        /////idMenu = 3
        // lấy danh sách sp  theo thương hiệu có phân trang, sắp xếp
        internal List<ProductViewModel> GetAllProductByBrand(int idBrand, int productPage, int page, int arrange)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();

            List<Product> lst_Products = mProductBus.GetAllProductByBrand(idBrand, productPage, page, arrange);
            foreach (Product p in lst_Products)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
        }

        // Lấy danh sách sp theo thương hiệu
        internal List<ProductViewModel> GetAllProductByBrand(int idBrand)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            List<Product> lst_Products = mProductBus.GetAllProductByBrand(idBrand);
            foreach (Product p in lst_Products)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
            
        }

        /////idMenu = 4
        // lấy danh sách sp  mới có phân trang, sắp xếp
        internal List<ProductViewModel> GetAllNewProduct(int productPage, int page, int arrange)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();

            List<Product> lstProduct = mProductBus.GetAllNewProduct(productPage, page, arrange);
            foreach (Product p in lstProduct)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }
            return result;
        }

        // lấy danh sách sp  mới
        internal List<ProductViewModel> GetAllNewProduct()
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            List<Product> lstProduct = mProductBus.GetAllNewProduct();
            foreach (Product p in lstProduct)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
            
        }


        /////idMenu = 5
        // lấy danh sách sp  khuyến mãi có phân trang, sắp xếp
        internal List<ProductViewModel> GetAllSaleOffProduct(int productPage, int page, int arrange)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();

            List<Product> lstProduct = new List<Product>();
            lstProduct = mProductBus.GetAllSaleOffProduct(productPage, page, arrange);
            foreach (Product p in lstProduct)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }
            return result;
        }

        //lấy danh sách sp  khuyến mãi
        internal List<ProductViewModel> GetAllSaleOffProduct()
        {
            List<ProductViewModel> result = new List<ProductViewModel>();

            List<Product> lstProduct = new List<Product>();
            lstProduct = mProductBus.GetAllSaleOffProduct();
            foreach (Product p in lstProduct)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }
            return result;
        }

        // Lấy danh sách sản phẩm cùng loại
        internal List<ProductViewModel> GetAllProduct(int idCategory)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            List<Product> lst_Products = mProductBus.GetAllProduct(idCategory);
            foreach (Product p in lst_Products)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
        }


        // Lấy tên thương hiệu theo idBrand
        internal string GetBrandName(int idBrand)
        {
            return mProductBus.GetBrandName(idBrand);
        }

        // Lấy tên loại theo idCategory
        internal string GetCategoryName(int idCategory)
        {
            return mProductBus.GetCategoryName(idCategory);
        }

        internal string GetColor(int idColor)
        {
            return mProductBus.GetColor(idColor);
        }

        internal string GetBrand(int idBrand)
        {
            return mProductBus.GetBrand(idBrand);
        }

        internal string GetSize(int idSize)
        {
            return mProductBus.GetSize(idSize);
        }

        internal string GetCategory(int idCategory)
        {
            return mProductBus.GetCategory(idCategory);
        }

        // Cập nhật lượt view
        internal string UpdateViewed(int idProduct)
        {
            return mProductBus.UpdateViewed(idProduct);
        }

        // Admin
        internal string AddProduct(string productName, int idBrand, int idCategory, int idColor, int idSize, 
            int saleOff, double price, int quantity, string describe, string image, string largeImage, int menWomen)
        {
            return mProductBus.AddProduct(productName, idBrand, idCategory, idColor, idSize, 
            saleOff, price, quantity, describe, image, largeImage, menWomen);
        }
        
        // Lấy danh sách sp có phân trang
        internal List<ViewProductViewModel> GetAllProduct(int productPage, int page)
        {
            List<ViewProductViewModel> result = new List<ViewProductViewModel>();
            List<ProductInfoPoco> lst_Products = mProductBus.GetAllProduct(productPage, page);
            foreach (ProductInfoPoco p in lst_Products)
            {
                ViewProductViewModel temp = new ViewProductViewModel();
                temp = ConvertToViewProductViewModel(p);

                result.Add(temp);
            }

            return result;
        }

        // Lấy tất cả sản phẩm
        internal List<ViewProductViewModel> GetAllProduct()
        {
            List<ViewProductViewModel> result = new List<ViewProductViewModel>();
            List<ProductInfoPoco> lst_Products = mProductBus.GetAllProduct();
            foreach (ProductInfoPoco p in lst_Products)
            {
                ViewProductViewModel temp = new ViewProductViewModel();
                temp = ConvertToViewProductViewModel(p);

                result.Add(temp);
            }

            return result;
        }

        // Xóa sản phẩm
        internal int DeleteProduct(int idProduct)
        {
            return mProductBus.DeleteProduct(idProduct);
        }

        // Cập nhật sản phẩm
        internal string EditProduct(int id, string productName, int idBrand, int idCategory, int idColor, int idSize,
            int saleOff, double price, int quantity, string describe, int viewed, int newProduct, string image, string largeImage, int menWomen)
        {
            return mProductBus.EditProduct(id, productName, idBrand, idCategory, idColor, idSize,
            saleOff, price, quantity, describe, viewed, newProduct, image, largeImage, menWomen);
        }


        // Tìm sản phẩm theo id
        internal AddProductViewModel FindProductById(int idProduct)
        {
            AddProductViewModel product = new AddProductViewModel();

            Product p = new Product();
            p = mProductBus.FindProductById(idProduct);

            product.ID = p.ID;
            product.ProductName = p.ProductName;
            product.IDCategory = p.IDCategory;
            product.IDSize = p.IDSize;
            product.IDColor = p.IDColor;
            product.IDBrand = p.IDBrand;
            product.SaleOff = p.SaleOff;
            product.Price = p.Price;
            product.Image = p.Image;
            product.Quantity = p.Quantity;
            product.Describe = p.Describe;
            product.NewProduct = p.NewProduct;
            product.Viewed = p.Viewed;
            product.LargeImage = p.LargeImage;
            product.Status = p.Status;
            product.MenWomen = p.MenWomen;

            return product;
        }




        
        /// <summary>
        /// Huy
        /// </summary>
        /// <returns></returns>

        public List<Brand> GetAllBrand()
        {
            //List<String> lstBrand = new List<String>();
            //lstBrand.Add("Nike");
            //lstBrand.Add("Adidas");
            List<Brand> lstBrand = new List<Brand>();
            lstBrand = mProductBus.GetAllBrand();
            //Sample Data
            //Brand brand1 = new Brand();
            //brand1.ID = 1;
            //brand1.BrandName = "Nike";
            //lstBrand.Add(brand1);
            //Brand brand2 = new Brand();
            //brand2.ID = 2;
            //brand2.BrandName = "Adidas";
            //lstBrand.Add(brand2);

            return lstBrand;
        }

        public List<Size> GetAllSize()
        {
            List<Size> lstSize = new List<Size>();
            //lstSize.Add("M");
            //lstSize.Add("L");
            lstSize = mProductBus.GetAllSize();
            //Sample Data
            //Size brand1 = new Size();
            //brand1.ID = 1;
            //brand1.SizeName = "M";
            //lstSize.Add(brand1);
            //Size brand2 = new Size();
            //brand2.ID = 2;
            //brand2.SizeName = "L";
            //lstSize.Add(brand2);

            return lstSize;
        }

        public List<Color> GetAllColor()
        {
            List<Color> lstColor = new List<Color>();
            lstColor = mProductBus.GetAllColor();
            //lstColor.Add("Red");
            //lstColor.Add("Green");
            //Sample Data
            //Color brand1 = new Color();
            //brand1.ID = 1;
            //brand1.ColorName = "Red";
            //lstColor.Add(brand1);
            //Color brand2 = new Color();
            //brand2.ID = 2;
            //brand2.ColorName = "Green";
            //lstColor.Add(brand2);

            return lstColor;
        }


        // Tìm danh sách sản phẩm có lượt view cao nhất (Home/Index)
        public List<ProductViewModel> GetAllProductByView()
        {
            List<ProductViewModel> result = new List<ProductViewModel>();

            List<Product> lst_Products = mProductBus.GetAllProductByView();
            foreach (Product p in lst_Products)
            {
                ProductViewModel temp = new ProductViewModel();
                temp = ConvertToViewModel(p);

                result.Add(temp);
            }

            return result;
        }
    }
}