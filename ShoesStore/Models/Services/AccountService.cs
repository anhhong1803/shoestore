﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;

namespace ShoesStore.Models.Services
{
    public class AccountService
    {
        private ShoesStoreBLL.BusServices.AccountBus mAccountBus;

        public AccountService()
        {
            this.mAccountBus = new AccountBus();
        }

        //public String Register(AccountViewModel accountViewModel)
        //{
        //    Account accountPoCo = new Account
        //    {
        //        Username = accountViewModel.Username,
        //        Password = accountViewModel.Password,
        //        Name = accountViewModel.Name,
        //        Gender = accountViewModel.Gender,
        //        Email = accountViewModel.Email,
        //        Birthday = accountViewModel.Birthday,
        //        Phone = accountViewModel.Phone,
        //        Address = accountViewModel.Address
        //    };

        //    return mAccountBus.Register(accountPoCo);
        //}

        //public String ConfirmAccount(String userName, String confirmCode)
        //{
        //    return mAccountBus.ConfirmAccount( userName, confirmCode);
        //}

        internal List<AccountViewModel> GetAllAccount(int numPerPage, int pageNum, string orderByField, bool descending = false)
        {
            List<User> lstAccountPoCo = mAccountBus.GetAllAccount(numPerPage, pageNum, orderByField, descending);

            List<AccountViewModel> lstAccountViewModel = new List<AccountViewModel>();
            foreach (var acc in lstAccountPoCo)
            {
                AccountViewModel newAcc = new AccountViewModel
                {
                    Username = acc.UserName,
                    Gender = acc.Gender,
                    Email = acc.Email,
                    Birthday = acc.BirthDay,
                    Phone = acc.PhoneNumber,
                    RegisteredDate = acc.RegisteredDate,
                    Address = acc.Address,
                    Name = acc.Name,
                    HasUse = acc.Status == 0
                };
                lstAccountViewModel.Add(newAcc);
            }

            return lstAccountViewModel;
        }

        public int CountAccount()
        {
            return mAccountBus.CountAccount();
        }

        public List<RoleViewModel> GetAllRoles()
        {
            List<Role> lstRolePoCo = mAccountBus.GetAllRoles();
            List<RoleViewModel> roles = new List<RoleViewModel>();
            foreach (var rolePoco in lstRolePoCo)
            {
                roles.Add(new RoleViewModel
                {
                    ID = rolePoco.Id,
                    NameRole = rolePoco.Name,
                    HasRole = false
                });
            }

            return roles;
        }

        public int Delete(string username)
        {
            return mAccountBus.Delete(username);
        }

        internal List<AccountViewModel> Search(string keyword, string searchfor,int numPerPage, int pageNum, string orderByField, bool descending = false)
        {
            List<User> lstAccountPoCo = mAccountBus.Search(keyword, searchfor,numPerPage, pageNum, orderByField, descending);

            List<AccountViewModel> lstAccountViewModel = new List<AccountViewModel>();
            foreach (var acc in lstAccountPoCo)
            {
                AccountViewModel newAcc = new AccountViewModel
                {
                    Username = acc.UserName,
                    Gender = acc.Gender,
                    Email = acc.Email,
                    Birthday = acc.BirthDay,
                    Phone = acc.PhoneNumber,
                    RegisteredDate = acc.RegisteredDate,
                    Address = acc.Address,
                    Name = acc.Name,
                    HasUse = acc.Status == 0
                };
                lstAccountViewModel.Add(newAcc);
            }

            return lstAccountViewModel;
        }
    }
}