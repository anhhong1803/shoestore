﻿using ShoesStore.Models.ViewModel;
using ShoesStoreBLL.BusServices;
using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.Services
{
    public class StatisticsService
    {
        StatisticsBus mStatisticsBus;

        public StatisticsService()
        {
            mStatisticsBus = new StatisticsBus();
        }

        public List<ObjectStatistics> StatisticsDay(DateTime day)
        {
            // Bổ sung thêm tháng và năm
            List<ObjectStatistics> lstObj = new List<ObjectStatistics>();
            // DAnh sách trả về các order trong vòng 7 ngày trước từ ngày truyền vào
            for(int i = 0; i < 7; i++)
            {
                List<Order> lstOrder = mStatisticsBus.StatisticsDay(day);
                lstObj.Add(new ObjectStatistics
                {
                    label = day.ToShortDateString(),
                    value = lstOrder.Sum(o=>o.OrderDetails.Sum(d=>d.Quantity))
                });
                day = day.AddDays(-1);
            }

            lstObj.Reverse();
            return lstObj;
        }

        public List<ObjectStatistics> StatisticsMonth(DateTime month)
        {
            List<ObjectStatistics> lstObj = new List<ObjectStatistics>();
            DateTime day = DateTime.Parse("01/01/" + month.Year);
            
            for (int i = 0; i < 12; i++)
            {
                List<Order> lstOrder = mStatisticsBus.StatisticsMonth(day);
                lstObj.Add(new ObjectStatistics
                {
                    label = day.Month.ToString() + "/" + day.Year.ToString(),
                    value = lstOrder.Sum(o => o.OrderDetails.Sum(d => d.Quantity))
                });
                day = day.AddMonths(1);
            }
            
            return lstObj;
        }

        public List<ObjectStatistics> StatisticsYear(DateTime year)
        {
            List<ObjectStatistics> lstObj = new List<ObjectStatistics>();

            DateTime day = year;
            for (int i = 0; i < 5; i++)
            {
                List<Order> lstOrder = mStatisticsBus.StatisticsYear(day);
                lstObj.Add(new ObjectStatistics
                {
                    label = day.Year.ToString(),
                    value = lstOrder.Sum(o => o.OrderDetails.Sum(d => d.Quantity))
                });
                day = day.AddYears(-1);
            }
            lstObj.Reverse();
            return lstObj;
        }

        public List<ProductStatistics> StatisticsTop10ProductDay(DateTime date)
        {
            List<ProductStatistics> lst = new List<ProductStatistics>();

            List<StatisticsProductBus> lstPro = mStatisticsBus.StatisticsTop10ProductDay(date);
            foreach (var pro in lstPro)
            {
                lst.Add(new ProductStatistics(pro.ID, pro.Name, pro.Count));
            }

            return lst;
        }

        public List<ProductStatistics> StatisticsTop10ProductMonth(DateTime date)
        {
            List<ProductStatistics> lst = new List<ProductStatistics>();

            List<StatisticsProductBus> lstPro = mStatisticsBus.StatisticsTop10ProductMonth(date);
            foreach (var pro in lstPro)
            {
                lst.Add(new ProductStatistics(pro.ID, pro.Name, pro.Count));
            }

            return lst;
        }

        public List<ProductStatistics> StatisticsTop10ProductYear(DateTime date)
        {
            List<ProductStatistics> lst = new List<ProductStatistics>();

            List<StatisticsProductBus> lstPro = mStatisticsBus.StatisticsTop10ProductYear(date);
            foreach (var pro in lstPro)
            {
                lst.Add(new ProductStatistics(pro.ID, pro.Name, pro.Count));
            }

            return lst;
        }
    }
}