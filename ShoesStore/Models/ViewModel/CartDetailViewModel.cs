﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ShoesStoreBLL.EF;

namespace ShoesStore.Models.ViewModel
{
    public class CartDetailViewModel
    {
        public List<CartItem> CartItems { get; set; }
        public int TotalQuanity { get; set; }
        public double TotalPrice { get; set; }
    }
}