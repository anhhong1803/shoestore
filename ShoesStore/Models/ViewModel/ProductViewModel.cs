﻿using System.ComponentModel.DataAnnotations;

namespace ShoesStore.Models.ViewModel
{
    public class ProductViewModel
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public int IDCategory { get; set; }
        [Display(Name = "Kích cỡ")]
        public int IDSize { get; set; }
        [Display(Name = "Màu sắc")]
        public int IDColor { get; set; }
        [Display(Name = "Thương hiệu")]
        public int IDBrand { get; set; }
        public int SaleOff { get; set; }
        public double Price { get; set; }
        public string Image { get; set; }
        public int Quantity { get; set; }
        public string Describe { get; set; }
        public int NewProduct { get; set; }
        public int Viewed { get; set; }
        public string LargeImage { get; set; }
        public int MenWomen { get; set; } 
        public int Status { get; set; }
    }

    public class AddProductViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Tên sản phẩm")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Loại")]
        public int IDCategory { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Nam/Nữ")]
        public int MenWomen { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Size")]
        public int IDSize { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Màu")]
        public int IDColor { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Thương hiệu")]
        public int IDBrand { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Khuyến mãi")]
        public int SaleOff { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Giá")]
        public double Price { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Mô tả")]
        public string Describe { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Số lượng")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Hình ảnh đại diện")]
        public string Image { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Hình ảnh chi tiết")]
        public string LargeImage { get; set; }

        [Required(ErrorMessage = "{0} không được rỗng")]
        [Display(Name = "Sản phẩm mới")]
        public int NewProduct { get; set; }

        public int Viewed { get; set; }
        public int Status { get; set; }
    }

    public class ViewProductViewModel
    {
        public int ID { get; set; }
        public string ProductName { get; set; }

        public string CategoryName { get; set; }
        public int IDCategory { get; set; }

        public string SizeName { get; set; }
        public int IDSize { get; set; }

        public string ColorName { get; set; }
        public int IDColor { get; set; }

        public string BrandName { get; set; }
        public int IDBrand { get; set; }

        public int SaleOff { get; set; }
        public double Price { get; set; }
        public string Image { get; set; }
        public int Quantity { get; set; }
        public string Describe { get; set; }
        public int NewProduct { get; set; }
        public int Viewed { get; set; }
        public string LargeImage { get; set; }
        public int Status { get; set; }
        public int MenWomen { get; set; }
    }

}