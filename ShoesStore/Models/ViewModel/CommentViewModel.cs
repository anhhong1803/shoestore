﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class CommentViewModel
    {
        public int ID { get; set; }
        
        public string Name { get; set; }
        public string Comment1 { get; set; }
        public System.DateTime Date { get; set; }
        public int IDProduct { get; set; }
        public int Status { get; set; }

    }
}