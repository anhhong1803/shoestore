﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class OrderViewModel
    {       
        public int ID { get; set; }
        public string IDUser { get; set; }
        public int IDOrderStatus { get; set; }
        public DateTime Date { get; set; }
        public double Total { get; set; }
        public int Status { get; set; }

        public string UserName { get; set; }
        public string OrderStatus { get; set; }
    }

    public class OrderDetailViewModel
    {
        public int ID { get; set; }
        public int IDOrder { get; set; }
        public int IDProduct { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }

        
        public string ProductName { get; set; }
        public string Image { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
    
    }
}