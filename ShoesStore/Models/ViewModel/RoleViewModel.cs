﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class RoleViewModel
    {
        public string ID { get; set; }
        public string NameRole { get; set; }
        public bool HasRole { get; set; }
    }
}