﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Tên Đăng Nhập không được rỗng")]
        [Display(Name = "Tên Đăng Nhập")]
        public String Username { get; set; }

        [Required(ErrorMessage = "Mật Khẩu không được rỗng")]
        [Display(Name = "Mật Khẩu")]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [Display(Name = "Ghi nhớ?")]
        public Boolean RememberMe { get; set; }
    }
}