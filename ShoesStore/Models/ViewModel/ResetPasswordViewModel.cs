﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class ResetPasswordViewModel
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Token { get; set; }

        [Required(ErrorMessage = "{0} phải từ 6-20 kí tự")]
        [Display(Name = "Mật Khẩu")]
        [DataType(DataType.Password)]
        [StringLength(maximumLength: 20, MinimumLength = 6, ErrorMessage = "{0} phải từ 6-20 kí tự")]
        public String Password { get; set; }

        [Required(ErrorMessage = "Mật khẩu nhập lại không được rỗng.")]
        [Display(Name = "Nhập Lại Mật Khẩu")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Mật khẩu nhập lại phải giống mật khẩu.")]
        public String ConfirmPassword { get; set; }
    }
}