﻿using System.ComponentModel.DataAnnotations;

namespace ShoesStore.Models.ViewModel
{
    public class CategoryViewModel
    {
        public int ID { get; set; }
        public string CategoryName { get; set; }
        public int Status { get; set; }
    }

    public class AddCategoryViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Tên loại sản phẩm không được rỗng")]
        [Display(Name = "Tên loại sản phẩm")]
        public string CategoryName { get; set; }

        public int Status { get; set; }
    }


}