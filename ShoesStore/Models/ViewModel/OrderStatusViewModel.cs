﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class OrderStatusViewModel
    {
        public int ID { get; set; }
        public string OrderStatus { get; set; }
     
    }
}