﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class ForgorPasswordViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập Tên đăng nhập hoặc Email")]
        [Display(Name = "Tên Đăng Nhập hoặc Email")]
        public String Username { get; set; }
    }
}