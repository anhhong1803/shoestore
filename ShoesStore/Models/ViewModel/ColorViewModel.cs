﻿namespace ShoesStore.Models.ViewModel
{
    public class ColorViewModel
    {
        public int ID { get; set; }
        public string ColorName { get; set; }
       
    }
}