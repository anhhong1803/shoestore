﻿namespace ShoesStore.Models.ViewModel
{
    public class BrandViewModel
    {
        public int ID { get; set; }
        public string BrandName { get; set; }
        
    }
}