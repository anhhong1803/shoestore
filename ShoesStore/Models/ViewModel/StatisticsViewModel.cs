﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class StatisticsViewModel
    {
        public StatisticsViewModel() { }

        public string key {get;set;}
        public List<ObjectStatistics> values { get; set; }
    }

    public class ObjectStatistics
    {
        public ObjectStatistics()
        { }
        public ObjectStatistics(string l, double v)
        {
            label = l;
            value = v;
        }
        public string label { get; set; }
        public double value { get; set; }
    }

    public class ProductStatistics
    {
        public ProductStatistics() { }
        public ProductStatistics(int id,string name, int count)
        {
            ID = id;
            Name = name;
            Count = count;
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}