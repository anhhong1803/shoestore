﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class EditProfileViewModel
    {
        [Required(ErrorMessage = "Tên của bạn không được rỗng")]
        [Display(Name = "Họ Tên")]
        public String Name { get; set; }

        public String Email { get; set; }

        [Display(Name = "Giới Tính")]
        public String Gender { get; set; }

        [Required(ErrorMessage = "Ngày sinh không được rỗng")]
        [Display(Name = "Ngày Sinh")]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        [Required(ErrorMessage = "Địa Chỉ không được rỗng")]
        [Display(Name = "Địa Chỉ")]
        public String Address { get; set; }

        [Required(ErrorMessage = "Điện thoại không được rỗng")]
        [Display(Name = "Điện Thoại")]
        [RegularExpression(@"(09\d{8})|(01\d{9})", ErrorMessage = "Số điện thoại không hợp lệ!")]
        public String Phone { get; set; }

        public List<RoleViewModel> ListRoleViewModel { get; set; }
    }
}