﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class CheckOutViewModel
    {
        [Required(ErrorMessage = "Tên của bạn không được rỗng")]
        [Display(Name = "Họ Tên")]
        public String Name { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email không hợp lệ.")]
        public String Email { get; set; }

        [Required(ErrorMessage = "Địa chỉ không được rỗng")]
        [Display(Name = "Địa Chỉ")]
        public String Address { get; set; }

        [Required(ErrorMessage = "Điện thoại không được rỗng")]
        [Display(Name = "Điện Thoại")]
        [RegularExpression(@"(09\d{8})|(01\d{9})", ErrorMessage = "Số điện thoại không hợp lệ!")]
        public String Phone { get; set; }

        [Display(Name = "Giống thông tin người đặt")]
        public Boolean IsOnePerson{ get; set; }
    }
}