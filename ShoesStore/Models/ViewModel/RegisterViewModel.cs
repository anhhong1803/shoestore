﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class RegisterViewModel 
    {
        [Required(ErrorMessage = "{0} phải từ 6-20 kí tự")]
        [Display(Name = "Tên Đăng Nhập")]
        [StringLength(maximumLength: 20, MinimumLength = 6, ErrorMessage = "{0} phải từ 6-20 kí tự")]
        public String Username { get; set; }

        [Required(ErrorMessage = "{0} phải từ 6-20 kí tự")]
        [Display(Name = "Mật Khẩu")]
        [DataType(DataType.Password)]
        [StringLength(maximumLength: 20, MinimumLength = 6, ErrorMessage = "{0} phải từ 6-20 kí tự")]
        public String Password { get; set; }

        [Required(ErrorMessage = "Mật khẩu nhập lại không được rỗng.")]
        [Display(Name = "Nhập Lại Mật Khẩu")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Mật khẩu nhập lại phải giống mật khẩu.")]
        public String ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Tên của bạn không được rỗng")]
        [Display(Name = "Họ Tên")]
        public String Name { get; set; }

        [Required(ErrorMessage = "Email không được rỗng")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email không hợp lệ.")]
        public String Email { get; set; }

        [Display(Name = "Giới Tính")]
        public String Gender { get; set; }

        [Required(ErrorMessage = "Ngày sinh không được rỗng")]
        [Display(Name = "Ngày Sinh")]
        public DateTime Birthday { get; set; }

        [Required(ErrorMessage = "Địa chỉ không được rỗng")]
        [Display(Name = "Địa Chỉ")]
        public String Address { get; set; }

        [Required(ErrorMessage = "Điện thoại không được rỗng")]
        [Display(Name = "Điện Thoại")]
        [RegularExpression(@"(09\d{8})|(01\d{9})", ErrorMessage = "Số điện thoại không hợp lệ!")]
        public String Phone { get; set; }

        public bool HasUse { get; set; }

        public DateTime? RegisteredDate { get; set; }

        //[Required(ErrorMessage = "Vui lòng nhập capcha!")]
        //public override string recaptcha_response_field { get; set; }
    }
}