﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ShoesStore.Models.ViewModel
{
    public class ResendConfirmMailViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập Tên tài khoản")]
        [Display(Name = "Tên Đăng Nhập")]
        public String Username { get; set; }
    }
}