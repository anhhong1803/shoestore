﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL
{
    public static class ErrorCodeDetail
    {
        public const string Err_Other = "Hệ thống có lỗi, vui lòng thử lại sau ít phút hoặc liên hệ chúng tôi để được hỗ trợ";
        //Register errors
        public const string Reg_Success = "Succesful";
        public const string Reg_UserNameConflict = "Tên đăng nhập đã tồn tại, vui lòng chọn tên khác!";
        public const string Reg_EmailConlict = "Email đã được đăng ký!";
        public const string Reg_UnconfirmedAccount = "Tên đăng nhập, Email này đã đăng ký nhưng chưa kích hoạt";
        
        //ConfirmAccount errors
        public const string ConAcc_Success = "Succesful";
        public const string ConAcc_WrongCode = "Mã kích hoạt không đúng, vui lòng kiểm tra lại!";
        public const string ConAcc_NotFound = "Mã kích hoạt hết hạn, vui lòng đăng ký như tài khoản mới";
    }
}
