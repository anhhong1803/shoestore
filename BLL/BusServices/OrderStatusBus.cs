﻿using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public class OrderStatusBus
    {
         private ShoeStoreEntities db;

         public OrderStatusBus()
        {
            this.db = new ShoeStoreEntities();
        }

        public List<OrderStatu> GetAllStatus()
        {
            var orderStatus = from s in db.OrderStatus
                              select s;
            return orderStatus.ToList();
        }

        public String GetStatusByIdOrder(int idOrder)
        {
            var status = from s in db.OrderStatus
                         join o in db.Orders on s.ID equals o.IDOrderStatus
                         where o.ID == idOrder
                         select s.OrderStatus;
            return status.First().ToString();
        }
    }
}
