﻿using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public class CategoryBus
    {
        private ShoeStoreEntities mShoeStoreDbContext;

        public CategoryBus()
        {
            this.mShoeStoreDbContext = new ShoeStoreEntities();
        }

        // Get list name of category
        public List<Category> GetListCategory()
        {
            List<Category> lstCategory = new List<Category>();

            var allCategory = from cate in mShoeStoreDbContext.Categories
                              where cate.Status == 0
                              select cate;

            lstCategory = allCategory.ToList<Category>();

            return lstCategory;
        }

        public List<Category> GetAllCategory(int numPerPage, int pageNum, string orderByField, bool descending)
        {
            IQueryable<Category> lstCategory = null;

            try
            {
                if (orderByField == "CategoryName")
                {

                    lstCategory = (from cate in mShoeStoreDbContext.Categories
                                   where cate.Status == 0
                                   orderby cate.CategoryName ascending
                                   select cate).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                       
                }

                return lstCategory.ToList();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }

        public List<Category> GetAllCategory()
        {
            List<Category> lstCategory = new List<Category>();

            var allCategory = from cate in mShoeStoreDbContext.Categories
                              where cate.Status == 0
                              select cate;
            
            lstCategory = allCategory.ToList<Category>();

            return lstCategory;
        }

        public string AddCategory(string categoryName)
        {
            string notice = null;
            // Kiểm tra tên loại sản phẩm có trùng hay không
            List<Category> lstCategory = new List<Category>();

            lstCategory = (from cate in mShoeStoreDbContext.Categories
                           where cate.Status == 0 && cate.CategoryName.Equals(categoryName)
                           select cate).ToList();
            if (lstCategory.Count == 0)
            {
                Category category = new Category
                {
                    CategoryName = categoryName,
                    Status = 0
                };

                mShoeStoreDbContext.Categories.Add(category);

                try
                {
                    mShoeStoreDbContext.SaveChanges();
                    notice = "Thêm mới thành công";
                }
                catch (Exception e)
                {
                    notice = "Thêm mới không thành công";
                }
            }
            else
            {
                notice = "Tên loại sản phẩm bị trùng";
            }
            
            return notice;
        }

        public List<Brand> GetAllBrand()
        {
            List<Brand> lstBrand = new List<Brand>();

            lstBrand = (from b in mShoeStoreDbContext.Brands
                        select b).ToList();

            return lstBrand;
        }

        public List<Color> GetAllColor()
        {
            List<Color> lstColor = new List<Color>();

            lstColor = (from c in mShoeStoreDbContext.Colors
                        select c).ToList();

            return lstColor;
        }

        public List<Size> GetAllSize()
        {
            List<Size> lstSize = new List<Size>();

            lstSize = (from c in mShoeStoreDbContext.Sizes
                        select c).ToList();

            return lstSize;
        }

        public int DeleteCategory(int id)
        {
            // Xóa sản phẩm thuộc category có id
            var product = from pro in mShoeStoreDbContext.Products
                          where pro.IDCategory == id && pro.Status == 0
                          select pro;
            foreach (var item in product)
            {
                item.Status = 1;
            }

            // Xóa loại sp
            var category = from p in mShoeStoreDbContext.Categories
                          where p.ID == id
                          select p;

            category.First().Status = 1;
            try
            {
                mShoeStoreDbContext.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        
        }

        public string EditCategory(int idCategory, string newCategoryName)
        {
            string notice = null;
            // Lấy category cũ
            Category category = new Category();

            category = (from cate in mShoeStoreDbContext.Categories
                        where cate.ID == idCategory
                        select cate).First();

            // Kiểm tra tên loại sản phẩm có trùng hay không với tên mới hay không
            List<Category> lstCategory = new List<Category>();

            lstCategory = (from cate in mShoeStoreDbContext.Categories
                           where cate.Status == 0 && cate.CategoryName.Equals(newCategoryName) && cate.ID != idCategory
                           select cate).ToList();
            if (lstCategory.Count == 0)
            {
                category.CategoryName = newCategoryName;
                try
                {
                    mShoeStoreDbContext.SaveChanges();
                    notice = "Cập nhật thành công";
                }
                catch (Exception e)
                {
                    notice = "Cập nhật không thành công";
                }
            }
            else
            {
                notice = "Tên loại sản phẩm bị trùng";
            }

            return notice;
        }



        public Category FindCategoryById(int idCategory)
        {
            Category category = new Category();

            category = (from cate in mShoeStoreDbContext.Categories
                        where cate.ID == idCategory && cate.Status == 0
                        select cate).First();

            return category;
        }
    }
}
