﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoesStoreBLL.EF;

namespace ShoesStoreBLL.BusServices
{
    public class CartBus
    {
        private ShoesStoreBLL.EF.ShoeStoreEntities shoeStoreEntity;

        public CartBus()
        {
            this.shoeStoreEntity = new ShoeStoreEntities();
        }

        public int AddToCart(string shoppingCartId, int procId, double price, int quantity, int sizeId, int colorId)
        {
            //Check if procId is Valid, check if colorid, sizeid is valid
            Product procduct = (shoeStoreEntity.Products.SingleOrDefault(p => p.ID == procId));
            if (procduct != null)
            {
                // Get the matching cart and album instances
                var cartItem = shoeStoreEntity.CartItems.SingleOrDefault(
    c => c.CartId == shoppingCartId
    && c.ItemId == procId);

                int maxQuantity = 0;
                int hasOrdered = 0;
                var cartItems = shoeStoreEntity.CartItems.Where(c => c.ItemId == procId);
                if (cartItems.Any())
                {
                    hasOrdered = cartItems.Sum(c => c.Quanity);
                }
                maxQuantity = procduct.Quantity - hasOrdered;

                if (maxQuantity > 0)
                {
                    if (cartItem == null)
                    {
                        // Create a new cart item if no cart item exists
                        cartItem = new CartItem
                        {
                            CartId = shoppingCartId,
                            ItemId = procId,
                            Quanity = quantity>maxQuantity?maxQuantity:quantity,
                            Price = price
                        };


                        shoeStoreEntity.CartItems.Add(cartItem);
                    }
                    else
                    {

                        cartItem.Quanity = quantity > maxQuantity ? maxQuantity : quantity;
                    }

                    // Save changes
                    shoeStoreEntity.SaveChanges();

                    return cartItem.Quanity;
                }
                    return 0;        
            }

            return 0;
        }

        public int RemoveFromCart(string shoppingCartId, int? procId)
        {
            if (procId == null)
            {
                return 0;
            }

            // Get the cart
            var cartItem = shoeStoreEntity.CartItems.SingleOrDefault(cart => cart.CartId == shoppingCartId && cart.ItemId == procId);

            if (cartItem != null)
            {
                shoeStoreEntity.CartItems.Remove(cartItem);

                // Save changes
                return shoeStoreEntity.SaveChanges();
            }

            return 0;
        }
        
        public int ChangeQuantity(string shoppingCartId, int itemId, int quantity)
        {
            CartItem cartItem = shoeStoreEntity.CartItems.SingleOrDefault(c => c.CartId == shoppingCartId && c.ItemId == itemId);

            if (cartItem != null)
            {
                if (quantity > 0 && quantity <= int.Parse(ConfigurationManager.AppSettings["MaxQuantityPerProduct"]))
                {
                    cartItem.Quanity = quantity;
                    shoeStoreEntity.SaveChanges();
                }

                return cartItem.Quanity;
            }

            return 0;
        }

        public List<CartItem> GetCartItems(string shoppingCartId)
        {
            try
            {
            IQueryable<CartItem> lstCartItem = (from item in shoeStoreEntity.CartItems
                                                  where item.CartId == shoppingCartId
                                                  select item);
            return lstCartItem.ToList();
            }
            catch (Exception)
            {
                
                return null;
            }
            
        }

        public void EmptyCart(string shoppingCartId)
        {
            shoeStoreEntity.CartItems.RemoveRange(shoeStoreEntity.CartItems.Where(i => i.CartId == shoppingCartId));
        }

        public Order CreateOrder(string shoppingCartId, string userId, Recipient rep=null)
        {
            IQueryable<CartItem> lstCartItem = (from item in shoeStoreEntity.CartItems
                                                where item.CartId == shoppingCartId
                                                select item);
            double totalPrice = 0;
            foreach (CartItem item in lstCartItem)
            {
                Product product = shoeStoreEntity.Products.FirstOrDefault(p => p.ID == item.ItemId);
                if (product == null || item.Quanity > product.Quantity)
                {
                    return null;
                }
                totalPrice += item.Price*item.Quanity;
            }

            Order order = null;
            if (rep != null)
            {
                rep = shoeStoreEntity.Recipients.Add(rep);
                order= new Order()
                {
                    IDUser = userId,
                    Total = totalPrice,
                    Date = DateTime.Now,
                    IDOrderStatus = 1,
                    IDRecipient = rep.ID,
                    Status = 0

                };
                order = shoeStoreEntity.Orders.Add(order);
            }
            else
            {
                order = new Order()
                {
                    IDUser = userId,
                    Total = totalPrice,
                    Date = DateTime.Now,
                    IDOrderStatus = 1,
                    Status = 0

                };
                order = shoeStoreEntity.Orders.Add(order);
            }
            

            foreach (CartItem item in lstCartItem)
            {
                OrderDetail orderDetail = new OrderDetail()
                {
                    IDOrder = order.ID,
                    IDProduct = item.ItemId,
                    Price = item.Price,
                    Quantity = item.Quanity
                };
                shoeStoreEntity.OrderDetails.Add(orderDetail);
                Product product = shoeStoreEntity.Products.FirstOrDefault(p => p.ID == item.ItemId);
                if (product != null) product.Quantity -= orderDetail.Quantity;
                shoeStoreEntity.CartItems.Remove(item);
            }

            shoeStoreEntity.SaveChanges();
            return order;
        }
    }
}
