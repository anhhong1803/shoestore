﻿using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public class StatisticsBus
    {
        private ShoeStoreEntities mShoeStoreDbContext;

        public StatisticsBus()
        {
            this.mShoeStoreDbContext = new ShoeStoreEntities();
        }

        public List<Order> StatisticsDay(DateTime day)
        {
            var orders = from order in mShoeStoreDbContext.Orders
                         where order.Date.Day == day.Day && order.Date.Month == day.Month && order.Date.Year == day.Year && order.Status == 0
                         select order;
            return orders.ToList();
        }


        public List<Order> StatisticsMonth(DateTime month)
        {
            
            var orders = from order in mShoeStoreDbContext.Orders
                         where order.Date.Month == month.Month && order.Status == 0
                         select order;
            return orders.ToList();
        }

        public List<Order> StatisticsYear(DateTime year)
        {

            var orders = from order in mShoeStoreDbContext.Orders
                         where order.Date.Year == year.Year && order.Status == 0
                         select order;
            // Chỉnh lại chỉ lấy trong vòng 5 năm thôi
            return orders.ToList();
        }

        public List<StatisticsProductBus> StatisticsTop10ProductDay(DateTime day)
        {
            try
            {

                var oders = from order in mShoeStoreDbContext.Orders
                            join orderDetail in mShoeStoreDbContext.OrderDetails on order.ID equals orderDetail.IDOrder
                            where order.Date.Day == day.Day
                            select new { orderDetail.ID, orderDetail.IDProduct };

                var idPros = oders.GroupBy(x => new { x.IDProduct }).Select(p => new { IDPro = p.Key.IDProduct, InstanceCount = p.Count() }).OrderByDescending(x => x.InstanceCount).Take(10);

                var pros = from pro in mShoeStoreDbContext.Products
                           join idpro in idPros on pro.ID equals idpro.IDPro
                           select new { pro.ID, pro.ProductName, idpro.InstanceCount };

                //var pros = idPros.Join(mShoeStoreDbContext.Products, p => p.IDProduct, id => id.ID, (p, id) => new { ID = id.ID, Name = id.ProductName, Count = p.InstanceCount });

                List<StatisticsProductBus> product = new List<StatisticsProductBus>();

                foreach( var pro in pros)
                {
                    product.Add(new StatisticsProductBus(pro.ID,pro.ProductName,pro.InstanceCount));
                }

                return product.ToList();
             }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }

        public List<StatisticsProductBus> StatisticsTop10ProductMonth(DateTime day)
        {
            try
            {

                var oders = from order in mShoeStoreDbContext.Orders
                            join orderDetail in mShoeStoreDbContext.OrderDetails on order.ID equals orderDetail.IDOrder
                            where order.Date.Month == day.Month
                            select new { orderDetail.ID, orderDetail.IDProduct };

                var idPros = oders.GroupBy(x => new { x.IDProduct }).Select(p => new { IDPro = p.Key.IDProduct, InstanceCount = p.Count() }).OrderByDescending(x => x.InstanceCount).Take(10);

                var pros = from pro in mShoeStoreDbContext.Products
                           join idpro in idPros on pro.ID equals idpro.IDPro
                           select new { pro.ID, pro.ProductName, idpro.InstanceCount };

                //var pros = idPros.Join(mShoeStoreDbContext.Products, p => p.IDProduct, id => id.ID, (p, id) => new { ID = id.ID, Name = id.ProductName, Count = p.InstanceCount });

                List<StatisticsProductBus> product = new List<StatisticsProductBus>();

                foreach (var pro in pros)
                {
                    product.Add(new StatisticsProductBus(pro.ID, pro.ProductName, pro.InstanceCount));
                }

                return product.ToList();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }

        public List<StatisticsProductBus> StatisticsTop10ProductYear(DateTime day)
        {
            try
            {

                var oders = from order in mShoeStoreDbContext.Orders
                            join orderDetail in mShoeStoreDbContext.OrderDetails on order.ID equals orderDetail.IDOrder
                            where order.Date.Year == day.Year
                            select new { orderDetail.ID, orderDetail.IDProduct };

                var idPros = oders.GroupBy(x => new { x.IDProduct }).Select(p => new { IDPro = p.Key.IDProduct, InstanceCount = p.Count() }).OrderByDescending(x => x.InstanceCount).Take(10);

                var pros = from pro in mShoeStoreDbContext.Products
                           join idpro in idPros on pro.ID equals idpro.IDPro
                           select new { pro.ID, pro.ProductName, idpro.InstanceCount };

                //var pros = idPros.Join(mShoeStoreDbContext.Products, p => p.IDProduct, id => id.ID, (p, id) => new { ID = id.ID, Name = id.ProductName, Count = p.InstanceCount });

                List<StatisticsProductBus> product = new List<StatisticsProductBus>();

                foreach (var pro in pros)
                {
                    product.Add(new StatisticsProductBus(pro.ID, pro.ProductName, pro.InstanceCount));
                }

                return product.ToList();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }


        }
    }
    public class StatisticsProductBus
    {
        public int ID {get;set;}
        public string Name {get;set;}
        public int Count {get;set;}

        public StatisticsProductBus(int id, string name, int count)
        {
            ID = id;
            Name = name;
            Count = count;
        }
    }
}
