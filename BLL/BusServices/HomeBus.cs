﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoesStoreBLL.EF;

namespace ShoesStoreBLL.BusServices
{
    public class HomeBus
    {
        private ShoeStoreEntities mShoeStoreDbContext;

        public HomeBus()
        {
            this.mShoeStoreDbContext = new ShoeStoreEntities();
        }

        // Action Search:
        public List<Product> AdSearch(ref int numProduct, int numProductPerPage, int brand, int size, double price, int color, int indPage)
        {
            int skip = (indPage - 1) * numProductPerPage;
            
            var lstProduct = from pro in mShoeStoreDbContext.Products
                             //orderby pro.ProductName ascending
                             select pro;

            if(brand != 0)
            {
                lstProduct = lstProduct.Where(pro => pro.IDBrand == brand);
            }
            if (size != 0)
            {
                lstProduct = lstProduct.Where(pro => pro.IDSize == size);

            }
            if (price != 0)
            {
                lstProduct = lstProduct.Where(pro => pro.Price <= price);
            }
            if (color != 0)
            {
                lstProduct = lstProduct.Where(pro => pro.IDColor == color);
            }

            numProduct = lstProduct.ToList().Count();
            return (lstProduct.AsQueryable().OrderBy(pro => pro.ProductName)).Skip(skip).Take(numProductPerPage).ToList(); 
        }

        public List<Product> Search(ref int numProduct, int numProductPerPage, string name, int indPage)
        {
            int skip = (indPage - 1) * numProductPerPage;

            var lstProduct = from pro in mShoeStoreDbContext.Products
                             where pro.ProductName.Contains(name)
                             //orderby pro.ProductName ascending
                             select pro;
            numProduct = lstProduct.ToList().Count();
            return (lstProduct.AsQueryable().OrderBy(pro => pro.ProductName)).Skip(skip).Take(3).ToList();
        }
    }
}
