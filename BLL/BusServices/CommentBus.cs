﻿using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public class CommentBus
    {
        private ShoeStoreEntities db;

        public CommentBus()
        {
            db = new ShoeStoreEntities();
        }

        public string AddComment(Comment comment)
        {
            string notice = null;
            try
            {
                db.Comments.Add(comment);

                db.SaveChanges();
                notice = "Thêm mới thành công";
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);
                notice = "Thêm mới không thành công";
            }

            return notice;
        }

        public List<Comment> GetAllComment(int idProduct)
        {
            var comments = from comment in db.Comments
                           orderby comment.Date descending
                           where comment.IDProduct == idProduct && comment.Status == 0
                           select comment;

            return comments.ToList();
        }
    }
}
