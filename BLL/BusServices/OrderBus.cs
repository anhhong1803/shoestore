﻿using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public class OrderBus
    {
        private ShoeStoreEntities db;

        public OrderBus()
        {
            this.db = new ShoeStoreEntities();
        }

        // Đếm số đơn hàng
        public int CountOrder()
        {
           
            try
            {
                return db.Orders.Count();            
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        
        }

        // Lấy danh sách đơn hàng sắp xếp, phân trang
        public List<Order> GetAllOrder(int numPerPage, int pageNum, string orderByField, bool descending)
        {
            IQueryable<Order> lstOrderPoco = null;
            
            try
            {
                switch (orderByField)
                {
                    case "Date":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0
                                        orderby order.Date
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "User":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0
                                        orderby order.IDUser
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "Status":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0
                                        orderby order.OrderStatu
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "Total":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0
                                        orderby order.Total
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    //IDOrder
                    default:
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0
                                        orderby order.ID
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                }

                return lstOrderPoco.ToList();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }

        }

        // Cập nhật tình trạng đơn hàng
        public string UpdateOrderStatus(int idOrder, int status)
        {
            string notice = null;
            try
            {
                // Lấy thông tin đơn hàng cũ
                Order order = new Order();
                order = (from o in db.Orders
                         where o.ID == idOrder && o.Status == 0
                         select o).First();

                 // Kiểm tra đơn hàng có bị hủy hay không
                if (status == 6)
                {

                    db.OrderDetails.RemoveRange(from d in db.Orders
                                                join od in db.OrderDetails on d.ID equals od.IDOrder
                                                where d.Status == 0 && d.ID == idOrder
                                                select od);
                    order.Status = 1;
                }

                order.IDOrderStatus = status;
               

                db.SaveChanges();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }

            return notice;
        }

        // Lấy danh sách chi tiết đơn hàng
        public List<OrderDetail> GetOrderDetail(int idOrder)
        {
            
            try
            {
                var orderDetail = from temp in db.OrderDetails
                                  where temp.IDOrder == idOrder
                                  select temp;
                return orderDetail.ToList();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }

        }


        // Lấy đơn hàng bằng id
        public Order GetOrderByID(int idOrder)
        {
            try
            {
                return db.Orders.Find(idOrder);
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }

        // Lấy danh sách đơn hàng sắp xếp, phân trang
        public List<Order> GetAllOrderOfUser(int numPerPage, int pageNum, string orderByField, bool descending, string idUser)
        {
            IQueryable<Order> lstOrderPoco = null;

            try
            {
                switch (orderByField)
                {
                    case "Date":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDUser == idUser
                                        orderby order.Date
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;  
                    case "Status":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDUser == idUser
                                        orderby order.OrderStatu
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "Total":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDUser == idUser
                                        orderby order.Total
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    //IDOrder
                    default:
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDUser == idUser
                                        orderby order.ID
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                }

                return lstOrderPoco.ToList();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);
            }
        }

        // Số đơn hàng của 1 khách hàng
        public int CountOrderOfUser(string idUser)
        {
            try
            {
                var count = from order in db.Orders
                            where order.IDUser == idUser && order.Status == 0
                            select order;
                            
                return count.Count();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }

        public string DeleteOrderByUser(int idOrder)
        {
            string notice = null;
            try
            {
                // Lấy thông tin đơn hàng cũ
                Order order = new Order();
                order = (from o in db.Orders
                         where o.ID == idOrder && o.Status == 0
                         select o).First();

                db.OrderDetails.RemoveRange(from d in db.Orders
                                            join od in db.OrderDetails on d.ID equals od.IDOrder
                                            where d.Status == 0 && d.ID == idOrder
                                            select od);
                order.Status = 1;
                

                order.IDOrderStatus = 5;


                db.SaveChanges();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }

            return notice;
        }

        // Lấy danh sách đơn hàng theo tình trạng phân trang, sắp xếp
        public List<Order> GetAllOrderByStatus(int numPerPage, int pageNum, string orderByField, int orderStatus, bool descending)
        {
            IQueryable<Order> lstOrderPoco = null;

            try
            {
                switch (orderByField)
                {
                    case "Date":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDOrderStatus == orderStatus
                                        orderby order.Date
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "User":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDOrderStatus == orderStatus
                                        orderby order.IDUser
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "Status":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDOrderStatus == orderStatus
                                        orderby order.OrderStatu
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    case "Total":
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDOrderStatus == orderStatus
                                        orderby order.Total
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                    //IDOrder
                    default:
                        lstOrderPoco = (from order in db.Orders
                                        where order.Status == 0 && order.IDOrderStatus == orderStatus
                                        orderby order.ID
                                        select order).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                        break;
                }

                return lstOrderPoco.ToList();

            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }

        // Đếm số đơn hàng theo tình trạng
        public int CountOrderByStatus(int orderStatus)
        {
            try
            {
                var count = from order in db.Orders
                            where order.Status == 0 && order.IDOrderStatus == orderStatus
                            select order;

                return count.Count();
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);

            }
        }
    }
}
