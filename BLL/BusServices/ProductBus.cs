﻿using ShoesStoreBLL.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public class ProductBus
    {
        private ShoeStoreEntities db;

        public ProductBus()
        {
            this.db = new ShoeStoreEntities();
        }

        // lấy danh sách sp  theo loại sản phẩm 
        public List<Product> GetAllProductByCategory(int idCategory, int idMenu)
        {
            //join p in mShoeStoreDbContext.Products on cate.ID equals p.IDCategory
            if (idMenu == 2)
            {
                idMenu = 0;
            }
            var product =  from pro in db.Products
                           where pro.IDCategory == idCategory  && pro.Status == 0 && pro.MenWomen == idMenu
                           select pro;
            return product.ToList();
        }

        // lấy danh sách sp  theo loại sản phẩm có phân trang, sắp xếp
        public List<Product> GetAllProductByCategory(int idCategory, int idMenu, int productPage, int page, int arrange)
        {
            if (idMenu == 2)
            {
                idMenu = 0;
            }

            List<EF.Product> product = new List<Product>();
            if (productPage != -1)
            {
                int n = (page - 1) * productPage;
                int m = productPage;

                
                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.ProductName ascending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.SaleOff descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).Skip(n).Take(m).ToList();
                }
            }
            else
            {
                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.ProductName ascending
                               select pro).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.SaleOff descending
                               select pro).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.IDCategory == idCategory && pro.Status == 0 && pro.MenWomen == idMenu
                               orderby pro.Price * (1 - pro.SaleOff / 100.0)  ascending
                               select pro).ToList();
                }
            }
            return product;   
        }

        // lấy danh sách sp  theo thương hiệu có phân trang, sắp xếp
        public List<Product> GetAllProductByBrand(int idBrand, int productPage, int page, int arrange)
        {
            List<EF.Product> product = new List<Product>();
            if (productPage != -1)
            {
                int n = (page - 1) * productPage;
                int m = productPage;

                
                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.ProductName ascending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.SaleOff descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).Skip(n).Take(m).ToList();
                }
            }
            else
            {
                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.ProductName ascending
                               select pro).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.SaleOff descending
                               select pro).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.IDBrand == idBrand && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).ToList();
                }
            }
            return product;   
        
        }

        // Láy danh sách sp theo thương hiệu
        public List<Product> GetAllProductByBrand(int idBrand)
        {
            var product = from pro in db.Products
                          where pro.IDBrand == idBrand && pro.Status == 0
                          select pro;
            return product.ToList();
        }

        // lấy danh sách sp  mới có phân trang, sắp xếp
        public List<Product> GetAllNewProduct(int productPage, int page, int arrange)
        {
            List<EF.Product> product = new List<Product>();
            if (productPage != -1)
            {
                int n = (page - 1) * productPage;
                int m = productPage;


                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.ProductName ascending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.SaleOff descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).Skip(n).Take(m).ToList();
                }
            }
            else
            {
                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.ProductName ascending
                               select pro).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.SaleOff descending
                               select pro).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.NewProduct == 1 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).ToList();
                }
            }
            return product;   
        
        }


        // lấy danh sách sp  mới
        public List<Product> GetAllNewProduct()
        {
            var product = from pro in db.Products
                          where pro.NewProduct == 1 && pro.Status == 0
                          select pro;
            return product.ToList();
        }

        // lấy danh sách sp  khuyến mãi có phân trang, sắp xếp
        public List<Product> GetAllSaleOffProduct(int productPage, int page, int arrange)
        {
            List<EF.Product> product = new List<Product>();
            if (productPage != -1)
            {
                int n = (page - 1) * productPage;
                int m = productPage;


                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.ProductName ascending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.SaleOff descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).Skip(n).Take(m).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).Skip(n).Take(m).ToList();
                }
            }
            else
            {
                // sắp xếp theo tên a- z
                if (arrange == 1)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.ProductName ascending
                               select pro).ToList();
                }

                // sắp xếp theo khuyến mãi
                if (arrange == 2)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.SaleOff descending
                               select pro).ToList();
                }

                // sắp xếp theo giá giảm dần
                if (arrange == 3)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) descending
                               select pro).ToList();
                }

                // sắp xếp theo giá tăng dần
                if (arrange == 4)
                {
                    product = (from pro in db.Products
                               where pro.SaleOff > 0 && pro.Status == 0
                               orderby pro.Price * (1 - pro.SaleOff / 100.0) ascending
                               select pro).ToList();
                }
            }
            return product;   
        
        }

        // lấy danh sách sp  khuyến mãi
        public List<Product> GetAllSaleOffProduct()
        {
            var product = from pro in db.Products
                          where pro.SaleOff > 0 && pro.Status == 0
                          select pro;
            return product.ToList();
        }

        // Lấy danh sách sản phẩm cùng loại
        public List<Product> GetAllProduct(int idCategory)
        {
            var product = from pro in db.Products
                          where pro.IDCategory == idCategory && pro.Status == 0
                          select pro;
            return product.ToList();
        }
        // Lấy tên thương hiệu bằng idBrand
        public string GetBrandName(int idBrand)
        {
            string brandName = (from brand in db.Brands
                                where brand.ID == idBrand
                                select brand.BrandName).First();

            return brandName;
        }

        public List<Brand> GetAllBrand()
        {
            var lstBrand = from brand in db.Brands
                           select brand;
            return lstBrand.ToList();
        }

        public List<Size> GetAllSize()
        {
            var lstSize = from size in db.Sizes
                          select size;
            return lstSize.ToList();
        }

        public List<Color> GetAllColor()
        {
            var lstColor = from color in db.Colors
                           select color;
            return lstColor.ToList();
        }

        public Product GetProduct(int idProduct)
        {
            return db.Products.Find(idProduct);
        }

        public string GetCategoryName(int idCategory)
        {
            string categoryName = (from category in db.Categories
                                   where category.ID == idCategory 
                                   select category.CategoryName).First();

            return categoryName;
        }

        public string GetColor(int idColor)
        {
            string color = (from c in db.Colors
                            where c.ID == idColor
                            select c.ColorName).First();

            return color;
        }

        public string GetBrand(int idBrand)
        {
            string brand = (from b in db.Brands
                            where b.ID == idBrand
                            select b.BrandName).First();

            return brand;
        }

        public string GetSize(int idSize)
        {
            string size = (from s in db.Sizes
                            where s.ID == idSize
                            select s.SizeName).First();

            return size;
        }

        public string GetCategory(int idCategory)
        {
            string category = (from c in db.Categories
                            where c.ID == idCategory
                            select c.CategoryName).First();

            return category;
        }

        public string AddProduct(string productName, int idBrand, int idCategory, int idColor, int idSize, int saleOff, double price, int quantity, string describe, string image, string largeImage, int menWomen)
        {
            string notice = null;
            try { 
                Product pro = new Product();
                pro.ProductName = productName;
                pro.IDCategory = idCategory;
                pro.IDSize = idSize;
                pro.IDColor = idColor;
                pro.IDBrand = idBrand;
                pro.SaleOff = saleOff;
                pro.Price = price;
                pro.Image = image;
                pro.Quantity = quantity;
                pro.Describe = describe;
                pro.NewProduct = 1;
                pro.Viewed = 0;
                pro.LargeImage = largeImage;
                pro.MenWomen = menWomen;
                pro.Status = 0;
                  

                db.Products.Add(pro);

                db.SaveChanges();
                notice = "Thêm mới thành công";
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);
                notice = "Thêm mới không thành công";
            }
            
            return notice;
        }

      

        public List<ProductInfoPoco> GetAllProduct()
        {
            
            var p =    from pro in db.Products
                       join c in db.Categories on pro.IDCategory equals c.ID
                       join co in db.Colors on pro.IDColor  equals co.ID
                       join b in db.Brands on pro.IDBrand equals b.ID
                       join s in db.Sizes on pro.IDSize equals s.ID
                       where pro.Status == 0
                       select new ProductInfoPoco{
                           ID = pro.ID,
                           ProductName = pro.ProductName,
                           CategoryName = c.CategoryName,
                           IDCategory = pro.IDCategory,
                           SizeName = s.SizeName,
                           IDSize = pro.IDSize,
                           ColorName = co.ColorName,
                           IDColor = pro.IDColor,
                           BrandName = b.BrandName,
                           IDBrand = pro.IDBrand,
                           SaleOff = pro.SaleOff,
                           Price = pro.Price,
                           Image = pro.Image,
                           Quantity = pro.Quantity,
                           Describe = pro.Describe,
                           NewProduct = pro.NewProduct,
                           Viewed = pro.Viewed,
                           LargeImage = pro.LargeImage,
                           MenWomen = pro.MenWomen,
                           Status = pro.Status

                       };
            return p.ToList();
        }



        public List<ProductInfoPoco> GetAllProduct(int productPage, int page)
        {
            int n = (page - 1) * productPage;
            int m = productPage;


            var p = (from pro in db.Products
                    join c in db.Categories on pro.IDCategory equals c.ID
                    join co in db.Colors on pro.IDColor equals co.ID
                    join b in db.Brands on pro.IDBrand equals b.ID
                    join s in db.Sizes on pro.IDSize equals s.ID
                    where pro.Status == 0
                    orderby pro.ProductName ascending
                    select new ProductInfoPoco()
                    {
                        ID = pro.ID,
                        ProductName = pro.ProductName,
                        CategoryName = c.CategoryName,
                        IDCategory = pro.IDCategory,
                        SizeName = s.SizeName,
                        IDSize = pro.IDSize,
                        ColorName = co.ColorName,
                        IDColor = pro.IDColor,
                        BrandName = b.BrandName,
                        IDBrand = pro.IDBrand,
                        SaleOff = pro.SaleOff,
                        Price = pro.Price,
                        Image = pro.Image,
                        Quantity = pro.Quantity,
                        Describe = pro.Describe,
                        NewProduct = pro.NewProduct,
                        Viewed = pro.Viewed,
                        LargeImage = pro.LargeImage,
                        Status = pro.Status,
                        MenWomen = pro.MenWomen
                    }).Skip(n).Take(m);

            return p.ToList();
        }

        // Xóa sản phẩm
        public int DeleteProduct(int idProduct)
        {
            
            var product =   from p in db.Products
                            where p.ID == idProduct
                            select p;

            product.First().Status = 1;
            try
            {
                db.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        
        }

        // tìm sản phẩm theo id
        public Product FindProductById(int idProduct)
        {
            Product product = new Product();

            product = (from p in db.Products
                        where p.ID == idProduct && p.Status == 0
                        select p).First();

            return product;
        }

        // Cập nhật sản phẩm
        public string EditProduct(int id, string productName, int idBrand, int idCategory, int idColor, int idSize, int saleOff, double price, 
            int quantity, string describe, int viewed, int newProduct, string image, string largeImage, int menWomen)
        {
            string notice = null;
            try
            {
                // Lấy thông tin sản phẩm cũ
                Product pro = new Product();
                pro = (from p in db.Products
                              where p.ID == id && p.Status == 0
                              select p).First();

                pro.ProductName = productName;
                pro.IDCategory = idCategory;
                pro.IDSize = idSize;
                pro.IDColor = idColor;
                pro.IDBrand = idBrand;
                pro.SaleOff = saleOff;
                pro.Price = price;
                pro.Image = image;
                pro.Quantity = quantity;
                pro.Describe = describe;
                pro.NewProduct = newProduct;
                pro.Viewed = viewed;
                pro.LargeImage = largeImage;
                pro.MenWomen = menWomen;
                pro.Status = 0;



                db.SaveChanges();
                notice = "Cập nhật thành công";
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);
                notice = "Cập nhật không thành công";
            }

            return notice;
        }

        // Cập nhật lượt view
        public string UpdateViewed(int idProduct)
        {
            string notice = null;
            try
            {
                // Lấy thông tin sản phẩm cũ
                Product pro = new Product();
                pro = (from p in db.Products
                       where p.ID == idProduct && p.Status == 0
                       select p).First();


                pro.Viewed = pro.Viewed + 1;
              
                db.SaveChanges();
               
            }
            catch (DbEntityValidationException e)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    sb.AppendLine(string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                    eve.Entry.Entity.GetType().Name,
                                                    eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName,
                                                    ve.ErrorMessage));
                    }
                }
                throw new DbEntityValidationException(sb.ToString(), e);
               
            }

            return notice;
        }

        // Tìm danh sách sản phẩm có view cao nhất
        public List<Product> GetAllProductByView()
        {
            List<Product> product = new List<Product>();
            product =     (from pro in db.Products
                          where pro.Status == 0
                           orderby pro.Viewed descending
                          select pro).ToList();

            return product;
        }
    }
}
