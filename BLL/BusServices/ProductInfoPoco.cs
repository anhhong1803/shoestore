﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoesStoreBLL.BusServices
{
    public partial class ProductInfoPoco
    {
        public int ID { get; set; }
        public string ProductName { get; set; }

        public string CategoryName { get; set; }
        public int IDCategory { get; set; }

        public string SizeName { get; set; }
        public int IDSize { get; set; }

        public string ColorName { get; set; }
        public int IDColor { get; set; }

        public string BrandName { get; set; }
        public int IDBrand { get; set; }
        public int SaleOff { get; set; }
        public double Price { get; set; }
        public string Image { get; set; }
        public int Quantity { get; set; }
        public string Describe { get; set; }
        public int NewProduct { get; set; }
        public int Viewed { get; set; }
        public string LargeImage { get; set; }
        public int Status { get; set; }
        public int MenWomen { get; set; }
    }
}
