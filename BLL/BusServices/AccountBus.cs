﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ShoesStoreBLL.EF;

namespace ShoesStoreBLL.BusServices
{
    public class AccountBus
    {
        private ShoeStoreEntities mShoeStoreDbContext;

        public AccountBus()
        {
            this.mShoeStoreDbContext = new ShoeStoreEntities();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accountPoCo"></param>
        /// <returns>
        ///     permission code if success
        ///    return result status
        /// </returns>
        //public String Register(Account accountPoCo)
        //{
        //    #region KiemTraTrongTaiKhoanDangHoatDong
        //    //Kiem tra trong Bang Account Chinh xem co trung ten dang nhap va email không
        //    var extinctAccount = from acc in mShoeStoreDbContext.Accounts
        //                         where (acc.Username == accountPoCo.Username || acc.Email == accountPoCo.Email)
        //                         select acc;

        //    //Neu trung TenDanhNhap hay MatKhau voi tkhoan da co, bao loi
        //    if (extinctAccount.ToList<Account>().Count != 0)
        //    {
        //        foreach (Account acc in extinctAccount.ToList<Account>())
        //        {
        //            if (System.String.Compare(acc.Username, accountPoCo.Username, System.StringComparison.Ordinal) == 0)
        //            {
        //                //tenDangNhap đã được đăng kí
        //                return ShoesStoreBLL.ErrorCodeDetail.Reg_UserNameConflict;
        //            }
        //            if (System.String.Compare(acc.Email, accountPoCo.Email, System.StringComparison.Ordinal) == 0)
        //            {
        //                //Email đã được đăng kí
        //                return ShoesStoreBLL.ErrorCodeDetail.Reg_EmailConlict;
        //            }
        //        }
        //    }
        //    #endregion KiemTraTrongTaiKhoanDangHoatDong
            
        //    //Kiem tra bảng UnconfirmedAccount
        //    #region KiemTraTrongTaiKhoanDangChoKichHoat
        //    //Kiem tra trong Bang Account Chinh xem co trung ten dang nhap va email không
        //    var uncomfirmedAccount = from acc in mShoeStoreDbContext.UnconfirmedAccounts
        //                         where (acc.Username == accountPoCo.Username || acc.Email == accountPoCo.Email)
        //                         select acc;

        //    //Neu tai khoan hop le, them vào bảng UnCònirm Account chờ xác nhận
        //    if (uncomfirmedAccount.ToList<UnconfirmedAccount>().Count != 0)
        //    {
        //        return ShoesStoreBLL.ErrorCodeDetail.Reg_UnconfirmedAccount;
        //    }
        //    #endregion KiemTraTrongTaiKhoanDangChoKichHoat
            
        //    //Tai Khoan Hop le, them vao bang UncomfirmedAccount
        //    accountPoCo.IDPermission = 1;
        //    accountPoCo.RegisteredDate = DateTime.Now;
        //    try
        //    {
        //        //Ran mã gồm 10 kí tự
        //        Random r = new Random();
        //        int temp = r.Next(10000, 99999);
        //        String conCode = temp.ToString();
        //        conCode += (r.Next(10000, 99999)).ToString();
        //        String toMail = accountPoCo.Email;

        //        //Gui  Ma Xac Nhan
        //        conCode = "0123456789";
        //        //SendConfirmMail(toMail, conCode);

        //    //Ghi xuong UnconfirmedAccount
        //        var idPermission = from permission in mShoeStoreDbContext.Permissions
        //            where System.String.Compare(permission.PermissionName, "user", System.StringComparison.Ordinal) == 0
        //            select permission.ID;

        //        ShoesStoreBLL.EF.UnconfirmedAccount unconfirmAcc = new UnconfirmedAccount
        //        {
        //            Username = accountPoCo.Username,
        //            Password = accountPoCo.Password,
        //            Name = accountPoCo.Name,
        //            Gender = accountPoCo.Gender,
        //            Email = accountPoCo.Email,
        //            Birthday = accountPoCo.Birthday,
        //            Phone = accountPoCo.Phone,
        //            Address = accountPoCo.Address,
        //            IDPermission = idPermission.First(),
        //            RegisteredDate = DateTime.Now,
        //            ConfirmCode = conCode,
        //            Status = 0
        //        };

        //        mShoeStoreDbContext.UnconfirmedAccounts.Add(unconfirmAcc);
        //        mShoeStoreDbContext.SaveChanges();
                
        //        return ShoesStoreBLL.ErrorCodeDetail.Reg_Success;
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.InnerException);
        //    }

        //    return ShoesStoreBLL.ErrorCodeDetail.Err_Other;
        //}

        //private static void SendConfirmMail(string toMail, string conCode)
        //{
        //    MailMessage mail = new MailMessage();
        //    mail.To.Add(toMail);
        //    mail.From = new MailAddress("tthieu2102@gmail.com");
        //    mail.Subject = "Activate Code";
        //    string Body = "Use " + conCode + " to activate your account!";
        //    mail.Body = Body;
        //    mail.IsBodyHtml = true;
        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "smtp.gmail.com";
        //    smtp.Port = 465;
        //    smtp.UseDefaultCredentials = false;
        //    smtp.Credentials = new System.Net.NetworkCredential
        //        ("justdoit.hcmus@gmail.com", "justdoit.12345"); // Enter seders User name and password
        //    smtp.EnableSsl = true;
        //    smtp.Send(mail);
        //}

        //public string ConfirmAccount(string userName, string confirmCode)
        //{
        //    var validAccount = from acc in mShoeStoreDbContext.UnconfirmedAccounts
        //                       where (acc.Username == userName)
        //                       select acc;

        //    //Neu confirm code dung
        //    if (validAccount.ToList<UnconfirmedAccount>().Count == 1)
        //    {
        //        if (System.String.Compare(validAccount.First().ConfirmCode, confirmCode, System.StringComparison.Ordinal) ==0)
        //        {
        //            Account newAcc = new Account
        //            {
        //                Username = validAccount.First().Username,
        //                Password = validAccount.First().Password,
        //                Gender = validAccount.First().Gender,
        //                Email = validAccount.First().Email,
        //                Birthday = validAccount.First().Birthday,
        //                Phone = validAccount.First().Phone,
        //                RegisteredDate = validAccount.First().RegisteredDate,
        //                IDPermission = validAccount.First().IDPermission,
        //                Address = validAccount.First().Address,
        //                Name = validAccount.First().Name,
        //                Status = 0
        //            };
        //            mShoeStoreDbContext.Accounts.Add(newAcc);
        //            mShoeStoreDbContext.UnconfirmedAccounts.Remove(validAccount.First());
        //            mShoeStoreDbContext.SaveChanges();
        //            return ShoesStoreBLL.ErrorCodeDetail.ConAcc_Success;
        //        }
        //        else
        //        {
        //            return ShoesStoreBLL.ErrorCodeDetail.ConAcc_WrongCode;
        //        }
                
        //    }
        //    else
        //    {
        //        return ShoesStoreBLL.ErrorCodeDetail.ConAcc_NotFound;
        //    }
        //}

        public List<User> GetAllAccount(int numPerPage, int pageNum, string orderByField, bool descending = false)
        {
            IQueryable<User> lstAccountPoCo = null;
            switch (orderByField)
            {
                case "RegisteredDate":
                    lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                      where acc.Status == 0
                                      orderby acc.RegisteredDate
                                      select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                    break;
                case "Name":
                    lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                      where acc.Status == 0
                                      orderby acc.Name
                                      select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                    break;
                case "Email":
                    lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                      where acc.Status == 0
                                      orderby acc.Email
                                      select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                    break;
                //Username
                default:
                    lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                      where acc.Status == 0
                                      orderby acc.Name
                                      select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                    break;
            }

            return lstAccountPoCo.ToList();

        }

        public List<Role> GetAllRoles()
        {
            IQueryable<Role> lstRolePoCo = from role in mShoeStoreDbContext.Roles
                                            select role;

            return lstRolePoCo.ToList();
        }

        public int CountAccount()
        {
            return mShoeStoreDbContext.Users.Count();
        }

        public int Delete(string username)
        {
            var user = from t in mShoeStoreDbContext.Users
                       where String.Compare(t.UserName, username, StringComparison.Ordinal) == 0
                       select t;

            user.First().Status = 1;
            try
            {
                mShoeStoreDbContext.SaveChanges();
                return 1;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }

        public List<User> Search(string keyword, string searchfor, int numPerPage, int pageNum, string orderByField, bool descending = false)
        {
            if (keyword == null) keyword = "";
            IQueryable<User> lstAccountPoCo = null;
            switch (searchfor)
            {
                case "Username":
                    switch (orderByField)
                    {
                        case "RegisteredDate":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.UserName.Contains(keyword)
                                              orderby acc.RegisteredDate
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Name":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.UserName.Contains(keyword)
                                              orderby acc.Name
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Email":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.UserName.Contains(keyword)
                                              orderby acc.Email
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        //Username
                        default:
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.UserName.Contains(keyword)
                                              orderby acc.UserName
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                    }
                    
                    break;
                case "Name":
                    switch (orderByField)
                    {
                        case "RegisteredDate":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Name.Contains(keyword)
                                              orderby acc.RegisteredDate
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Name":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Name.Contains(keyword)
                                              orderby acc.Name
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Email":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Name.Contains(keyword)
                                              orderby acc.Email
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        //Username
                        default:
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Name.Contains(keyword)
                                              orderby acc.UserName
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                    }
                    
                    break;
                case "Email":
                    switch (orderByField)
                    {
                        case "RegisteredDate":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Email.Contains(keyword)
                                              orderby acc.RegisteredDate
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Name":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Email.Contains(keyword)
                                              orderby acc.Name
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Email":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Email.Contains(keyword)
                                              orderby acc.Email
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        //Username
                        default:
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && acc.Email.Contains(keyword)
                                              orderby acc.UserName
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                    }
                    
                    break;
                //All
                default:
                    switch (orderByField)
                    {
                        case "RegisteredDate":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                                where acc.Status == 0 && (acc.UserName.Contains(keyword) || acc.Name.Contains(keyword) || acc.Email.Contains(keyword))
                                                orderby acc.RegisteredDate
                                                select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Name":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                                where acc.Status == 0 && (acc.UserName.Contains(keyword) || acc.Name.Contains(keyword) || acc.Email.Contains(keyword))
                                                orderby acc.Name
                                                select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        case "Email":
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                              where acc.Status == 0 && (acc.UserName.Contains(keyword) || acc.Name.Contains(keyword) || acc.Email.Contains(keyword))
                                              orderby acc.Email
                                              select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                        //Username
                        default:
                            lstAccountPoCo = (from acc in mShoeStoreDbContext.Users
                                                where acc.Status == 0 && (acc.UserName.Contains(keyword) || acc.Name.Contains(keyword) || acc.Email.Contains(keyword))
                                                orderby acc.UserName
                                                select acc).Skip((pageNum - 1) * numPerPage).Take(numPerPage);
                            break;
                    }
                    break;
            }

            return lstAccountPoCo.ToList();
        }
    }
}
